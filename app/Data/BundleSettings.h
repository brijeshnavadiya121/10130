//
//  BundleSettings.h
//  app
//
//  Created by Simon Moser on 05.04.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BundleSettings : NSObject
{
    BOOL _overrideRemoteApp;
}

@end
