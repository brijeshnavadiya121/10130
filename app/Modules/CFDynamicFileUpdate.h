//
//  CFDynamicFileUpdate.h
//  app
//
//  Created by LeyLa on 7/3/17.
//  Copyright © 2017 CloudFaces. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFDynamicFileUpdate : NSObject

+ (CFDynamicFileUpdate *)sharedInstance;
- (instancetype) init;
//- (void) downoadFileToLocalCacheFromSha1: (NSString *) stringSha1 andCopyTo: (NSString *) pathDir;
//- (void) renameFileWithName:(NSString *)srcName toName:(NSString *)dstName;
- (void) downoadManifest: (NSString *) stringSha1;

@property NSString *isManifest;

@property NSString *localCachePath;
@property NSString *errorMsg;
@property NSString *filePath;
@property NSString *isOK;
@property NSString *appVersion;
@property NSString *appVersionApiCall;

@end
