//
//  Contacts.m
//  app
//
//  Created by LeyLa Mehmed on 1/15/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "Contacts.h"

@implementation Contacts


+ (Contacts *)sharedInstance {
    static dispatch_once_t onceToken;
    static Contacts *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[Contacts alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
     //  [self contactScan];
      //  self.store = [[CNContactStore alloc] init];
    }
    return self;
}

- (void) contactScan
{
    CNAuthorizationStatus status = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts];
    
    if (status == CNAuthorizationStatusNotDetermined) {
        [self.store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError *error) {
            
            if (granted) {
                [self getContacts];
            }
        }];
    }
    else if(status == CNAuthorizationStatusAuthorized) {
        [self getContacts];
    }
}

- (void) getContacts {
    
        CNContactStore *store = [[CNContactStore alloc] init];

        NSArray *keys = @[CNContactIdentifierKey, CNContactGivenNameKey, CNContactFamilyNameKey];
    
        NSString *containerId = store.defaultContainerIdentifier;
        NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
        NSError *error;
        NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
        if (error) {
            NSLog(@"error fetching contacts %@", error);
        } else {

            _contacts = [[NSMutableArray alloc] init];
    
            for (CNContact *contact in cnContacts) {
                
                // copy data to my custom Contacts class.
       
                self.raw_id = [NSString stringWithFormat:@"%@", contact.identifier] ;
                self.name   = [NSString stringWithFormat:@"%@ %@", contact.givenName, contact.familyName];

                NSDictionary * personDict = [[NSDictionary alloc] initWithObjectsAndKeys:self.raw_id, @"raw_id", self.name,@"name", nil];
                
    
                [_contacts addObject:personDict];
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
        }

    }

- (void) getContactDetails :(NSString*) contactId{
    
    NSArray *contactsId = [contactId componentsSeparatedByString:@","];
    
    CNContactStore *store = [[CNContactStore alloc] init];

    //keys with fetching properties
    
    NSArray *keys = @[CNContactIdentifierKey, CNContactGivenNameKey, CNContactFamilyNameKey, CNContactNoteKey, CNContactUrlAddressesKey,CNContactPhoneNumbersKey,CNContactPostalAddressesKey,CNContactEmailAddressesKey];
 
   
    NSArray *contactsArray = [[NSArray alloc] initWithArray:contactsId];
    NSPredicate *predicate = [CNContact predicateForContactsWithIdentifiers:contactsArray];
    NSError *error;
    NSArray *cnContacts = [store unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
    if (error) {
        NSLog(@"error fetching contacts %@", error);
    } else {
        
        _contactDetails = [[NSMutableArray alloc] init];
        
        for (CNContact *contact in cnContacts) {
            
            NSMutableArray *websites  = [[NSMutableArray alloc] init];
            NSMutableArray *phones    = [[NSMutableArray alloc] init];
            NSMutableArray *addresses = [[NSMutableArray alloc] init];
            NSMutableArray *emails    = [[NSMutableArray alloc] init];

            // copy data to my custom Contacts class.
            
            self.raw_id = [NSString stringWithFormat:@"%@", contact.identifier] ;
            self.name   = [NSString stringWithFormat:@"%@ %@", contact.givenName, contact.familyName];
            self.note   = [NSString stringWithFormat:@"%@", contact.note] ;

     
            //get websites array
            for (CNLabeledValue *contactDataUrl in contact.urlAddresses) {
                NSLog(@"contactDataUrl %@", contactDataUrl);
                
                NSString *labelWithSymbols  = [NSString stringWithFormat:@"%@", contactDataUrl.label];
                NSString *labelType = [labelWithSymbols stringByReplacingOccurrencesOfString:@"_$!<" withString:@""];
                NSString *label = [labelType stringByReplacingOccurrencesOfString:@">!$_" withString:@""];
                
                NSDictionary *websitesData = [[NSDictionary alloc] initWithObjectsAndKeys:contactDataUrl.value, label,  nil];
                
                [websites addObject:websitesData];
            }
            
            //get phones array
            for (CNLabeledValue *contactDataPhones in contact.phoneNumbers) {
                
                NSString *labelWithSymbols  = [NSString stringWithFormat:@"%@", contactDataPhones.label];
                NSString *labelType = [labelWithSymbols stringByReplacingOccurrencesOfString:@"_$!<" withString:@""];
                NSString *label = [labelType stringByReplacingOccurrencesOfString:@">!$_" withString:@""];

                NSDictionary *phonesData = [[NSDictionary alloc] initWithObjectsAndKeys: [contactDataPhones.value stringValue], label,  nil];
                
                [phones addObject:phonesData];
            }
            
            //  get addresses array
            for (CNLabeledValue *contactDataAddresses in contact.postalAddresses) {
                
                NSString *labelWithSymbols  = [NSString stringWithFormat:@"%@", contactDataAddresses.label];
                NSString *labelType = [labelWithSymbols stringByReplacingOccurrencesOfString:@"_$!<" withString:@""];
                NSString *label = [labelType stringByReplacingOccurrencesOfString:@">!$_" withString:@""];
                
                _addrArr = [[NSMutableArray alloc]init];
                CNPostalAddressFormatter * formatter = [[CNPostalAddressFormatter alloc]init];
                NSArray * addressess = (NSArray*)[contact.postalAddresses valueForKey:@"value"];
                if (addressess.count > 0) {
                    for (CNPostalAddress* address1 in addressess) {
                        [_addrArr addObject:[formatter stringFromPostalAddress:address1]];
                    }
                }
                
                NSString *addressValueString = [_addrArr componentsJoinedByString:@","];
                NSString *newAddressValueString = [addressValueString stringByReplacingOccurrencesOfString:@"\n"
                                                     withString:@", "];
                NSDictionary *adressData = [[NSDictionary alloc] initWithObjectsAndKeys:newAddressValueString, label,  nil];
                
                [addresses addObject:adressData];
            }

           // get emails array
            for (CNLabeledValue *contactDataEmails in contact.emailAddresses) {
                
                NSString *labelWithSymbols  = [NSString stringWithFormat:@"%@", contactDataEmails.label];
                NSString *labelType = [labelWithSymbols stringByReplacingOccurrencesOfString:@"_$!<" withString:@""];
                NSString *label = [labelType stringByReplacingOccurrencesOfString:@">!$_" withString:@""];
                
                NSDictionary *emailData = [[NSDictionary alloc] initWithObjectsAndKeys: contactDataEmails.value, label,  nil];
                
                [emails addObject:emailData];
            }
            
            NSDictionary * personDict = [[NSDictionary alloc] initWithObjectsAndKeys: self.raw_id,@"raw_id", self.name, @"name",self.note, @"note", websites, @"websites", phones, @"phones", addresses, @"addresses", emails, @"emails",  nil];
            
            [_contactDetails addObject:personDict];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    }
    
}



@end
