//
//  NavigationWebViewStack.h
//  app
//
//  Created by LeyLa on 4/24/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NavigationWebViewStack : NSObject

@property (strong, nonatomic) NSMutableArray* webViews;

- (void) setWebView:(UIViewController* ) webView andPageId:(NSString*)pageId;

+ (instancetype) instance;
- (instancetype) init;

@end
