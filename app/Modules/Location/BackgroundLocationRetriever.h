//
//  LocationRetriever.h
//  app
//
//  Created by Bernhard Kunnert on 07.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface BackgroundLocationRetriever : NSObject

+ (instancetype) instance;

- (void) startLocationUpdates:(NSString*) url;
- (void) stopLocationUpdates;

- (NSArray*) getQueuedLocations;

@property(nonatomic, strong) NSString* isActive;

@end
