//
//  LocationRetriever.h
//  app
//
//  Created by Bernhard Kunnert on 07.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <WebKit/WebKit.h>

@protocol LocationRetrieverDelegate <NSObject>

- (void) locationManagerAuthorizationStatusListener:(NSString *) status;

@end
typedef void (^LocationUpdateBlock)(CLLocation*);
typedef void (^PermissionStatus)(NSString*);
typedef void (^CheckPermission)(BOOL);

@interface LocationRetriever : NSObject <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>
@property (weak, nonatomic)  id<LocationRetrieverDelegate> delegate;
- (void) checkPermission:(CheckPermission) status;
- (void) requestPermission:(PermissionStatus) permissionStatus;
- (void) updateCurrentLocation:(LocationUpdateBlock) updateBlock;

@property(nonatomic, strong) NSString *authorizationStatus;
@property BOOL updateLocation;

@end
