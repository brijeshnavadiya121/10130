//
//  LocationTracker.h
//  app
//
//  Created by LeyLa on 8/23/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationShareModel.h"

@interface LocationTracker : NSObject <CLLocationManagerDelegate>

@property (nonatomic) CLLocationCoordinate2D myLastLocation;
@property (nonatomic) CLLocationAccuracy myLastLocationAccuracy;

@property (strong,nonatomic) LocationShareModel * shareModel;

@property (nonatomic) CLLocationCoordinate2D myLocation;
@property (nonatomic) CLLocationAccuracy myLocationAccuracy;

+ (CLLocationManager *)sharedLocationManager;

- (void)startLocationTracking;
- (void)stopLocationTracking;
- (void)updateLocationToServer:(NSString *)startTrackingUrl;

//Get myLocationLatitude and myLocationLongitude

@property (strong, nonatomic) NSMutableString *myLocationLatitude;
@property (strong, nonatomic) NSMutableString *myLocationLongitude;

@property (nonatomic) NSMutableDictionary *myLocationDictInPlist;
@property (nonatomic) NSMutableArray *myLocationArrayInPlist;

@property (nonatomic) CLLocationManager * anotherLocationManager;
- (void)addResumeLocationToPList;

@property (nonatomic) BOOL afterResume;

//- (void)addApplicationStatusToPList:(NSString*)applicationStatus;
- (void) requestAuthorization;
- (void)startMonitoringLocation;
- (void)restartMonitoringLocation;
- (void)applicationEnterBackground;
@end
