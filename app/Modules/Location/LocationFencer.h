//
//  LocationFencer.h
//  app
//
//  Created by Bernhard Kunnert on 11.05.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationFencer : NSObject

@property (nonatomic, readonly) NSSet* monitoredRegions;

+ (instancetype) instance;

- (void) addObservedRegion:(CLRegion*) region;
- (void) removeObservedRegion:(NSString*) regionName;

@end
