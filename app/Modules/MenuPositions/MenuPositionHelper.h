//
//  MenuPositionHelper.h
//  app
//
//  Created by LeyLa Mehmed on 2/09/17.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuPositionHelper : NSObject

+ (MenuPositionHelper *)sharedInstance;
- (instancetype) init;

@property (strong, nonatomic) NSString* menuPosition;
@end
