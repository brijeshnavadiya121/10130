//
//  Dialog.h
//  app
//
//  Created by LeyLa Mehmed on 2/7/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebviewViewController.h"

@interface Dialog : NSObject {
    
    UITableView *NotificationTableView;
}

@property NSString *data;
@property NSString *title;
@property NSString *type;

+ (Dialog *)sharedInstance;
- (instancetype) init;

- (UIAlertController *) openDialogType:(NSString *)type title:(NSString *) title actions:(NSString *) actions data:(NSString *) data;

- (NSMutableDictionary *) getResultData;
@end
