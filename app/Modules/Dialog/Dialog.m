//
//  Dialog.m
//  app
//
//  Created by LeyLa Mehmed on 2/7/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "Dialog.h"
static const int PICKER_WIDTH = 320;
static const int PICKER_HEIGHT = 162;
static const int BUTTON_HEIGHT_20 = 20;

@implementation Dialog

NSMutableDictionary *radioButtonsData;

NSString *radioButtonId;
NSString *radioButtonLabel;
NSString *radioButtonValue;

NSMutableDictionary *checkBoxButtonsData;

NSString *checkBoxButtonId;
NSString *checkBoxButtonLabel;
NSString *checkBoxButtonValue;


UIButton *button;
NSInteger buttonTag;
NSString *buttonLabel;
NSString *buttonIdString;
NSMutableArray *buttonLabels;
NSMutableArray *radioButtonLabels;

NSString *enteredText;
NSString *textString;

NSString *typeInfo       = @"info";
NSString *typeText       = @"text";
NSString *typeRadio      = @"radio";
NSString *typeCheck      = @"check";
NSString *typeDate       = @"date";
NSString *typeTime       = @"time";

UIAlertController *alertController;

NSString *strDate;


+ (Dialog *)sharedInstance {
    static dispatch_once_t onceToken;
    static Dialog *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[Dialog alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(UIAlertController *) openDialogType:(NSString *)type title:(NSString *)title actions:(NSString *)actions data:(NSString *)data {
    
    _data  = data;
    _title = title;
    _type  = type;
    
    
    //Picker Dialog - date
    if ([type isEqualToString:typeDate]) {
        [self openDatePickerDialog];
    }
    
    //Picker Dialog - time
    if ([type isEqualToString:typeTime]) {
        [self openTimePickerDialog];
    }
    
    
    //Text Dialog
    if ([type isEqualToString:typeText]) {
        
        [self openTextDialog];
        
    }
    
    //Info Dialog
    if ([type isEqualToString:typeInfo]) {
        
        [self openInfoDialog];
    }
    
    //Check Dialog
    if ([type isEqualToString:typeCheck]) {
        [self openCheckBoxButtonsDialog];
        
        
    }
    
    //Radio Buttons Dialog
    if ([type isEqualToString:typeRadio]) {
        
        [self openRadioButtonsDialog];
    }
    
    return alertController;
    
}

#pragma mark openDatePickerDialog

- (void) openDatePickerDialog {
    if ((![_data isEqualToString:@""]) || (_data != nil)) {
        
        strDate = [[NSString alloc] init];
        NSError *jsonError;
        NSData *datas   = [_data dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *alertData = [NSJSONSerialization JSONObjectWithData:datas
                                                                  options:NSJSONReadingMutableContainers
                                                                    error:&jsonError];
        
        NSDictionary *datePickerData = alertData[@"picker"];
        
        UIViewController *controller = [[UIViewController alloc]init];
        UIView *alertView;
        CGRect rect;
        
        rect = CGRectMake(0, 0, PICKER_WIDTH, PICKER_HEIGHT);
        [controller setPreferredContentSize:rect.size];
        alertView  = [[UIView alloc]initWithFrame:rect];
        
        [controller.view addSubview:alertView];
        [controller.view bringSubviewToFront:alertView];
        [controller.view setUserInteractionEnabled:YES];
        [alertView setUserInteractionEnabled:YES];
        
        
        
        UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, PICKER_WIDTH, PICKER_HEIGHT)];
        
        
        if (![datePickerData[@"defaultValue"] isEqualToString: @"" ]) {
            
            NSString* dateString = datePickerData[@"defaultValue"];
            NSTimeInterval dateTimeInterval = [dateString doubleValue];
            NSDate * date = [NSDate dateWithTimeIntervalSince1970:dateTimeInterval /1000.0 ];
            
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setDateFormat:@"yyyy-MM-dd"];
            strDate = [_formatter stringFromDate:date];
            
            [picker setDate:date];
        }
        
        [picker setDatePickerMode:UIDatePickerModeDate];
        
        
        [picker addTarget:self action:@selector(datePickerChanged:) forControlEvents:UIControlEventValueChanged];
        [alertView addSubview:picker];
        
        
        alertController = [UIAlertController alertControllerWithTitle:_title message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController setValue:controller forKey:@"contentViewController"];
        
        
    }
    
}

#pragma mark openTimePickerDialog

- (void) openTimePickerDialog {
    if ((![_data isEqualToString:@""]) || (_data != nil)) {
        
        strDate = [[NSString alloc] init];
        NSError *jsonError;
        NSData *datas   = [_data dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *alertData = [NSJSONSerialization JSONObjectWithData:datas
                                                                  options:NSJSONReadingMutableContainers
                                                                    error:&jsonError];
        
        NSDictionary *datePickerData = alertData[@"picker"];
        
        UIViewController *controller = [[UIViewController alloc]init];
        UIView *alertView;
        CGRect rect;
        
        rect = CGRectMake(0, 0, PICKER_WIDTH, PICKER_HEIGHT);
        [controller setPreferredContentSize:rect.size];
        alertView  = [[UIView alloc]initWithFrame:rect];
        
        [controller.view addSubview:alertView];
        [controller.view bringSubviewToFront:alertView];
        [controller.view setUserInteractionEnabled:YES];
        [alertView setUserInteractionEnabled:YES];
        
        
        
        UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, PICKER_WIDTH, PICKER_HEIGHT)];
        
        
        if (![datePickerData[@"defaultValue"] isEqualToString: @"" ]) {
            
            NSString* dateString = datePickerData[@"defaultValue"];
            NSTimeInterval dateTimeInterval = [dateString doubleValue];
            NSDate * date = [NSDate dateWithTimeIntervalSince1970:dateTimeInterval / 1000.0];
            
            NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
            [_formatter setDateFormat:@"HH:mm"];
            strDate = [_formatter stringFromDate:date];
            
            [picker setDate:date];
            
        }
        [picker setDatePickerMode:UIDatePickerModeTime];
        
        
        [picker addTarget:self action:@selector(datePickerChangedTime:) forControlEvents:UIControlEventValueChanged];
        [alertView addSubview:picker];
        
        
        alertController = [UIAlertController alertControllerWithTitle:_title message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController setValue:controller forKey:@"contentViewController"];
        
    }
    
}

- (void)datePickerChanged:(UIDatePicker *)datePicker
{
    
    NSDate *date = datePicker.date;
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateFormat:@"yyyy-MM-dd"];
    strDate = [_formatter stringFromDate:date];
    
}

- (void)datePickerChangedTime:(UIDatePicker *)datePicker
{
    
    NSDate *date = datePicker.date;
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateFormat:@"HH:mm"];
    strDate = [_formatter stringFromDate:date];
    
}

#pragma mark openInfoDialog

- (void) openInfoDialog {
    
    NSError *jsonError;
    NSData *datas   = [_data dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *alertData = [NSJSONSerialization JSONObjectWithData:datas
                                                              options:NSJSONReadingMutableContainers
                                                                error:&jsonError];
    
    NSDictionary *textData = alertData[@"info"];
    textString = textData[@"label"];
    NSString *position = textData[@"position"];
    if (position) {
        if ([position isEqual:@"center"]) {
            alertController = [UIAlertController alertControllerWithTitle:_title message:textString preferredStyle:UIAlertControllerStyleAlert];
        }else{
            alertController = [UIAlertController alertControllerWithTitle:_title message:textString preferredStyle:UIAlertControllerStyleActionSheet];
        }
    } else{
        alertController = [UIAlertController alertControllerWithTitle:_title message:textString preferredStyle:UIAlertControllerStyleActionSheet];
    }
    
}

#pragma mark openTextDialog

- (void) openTextDialog {
    
    NSError *jsonError;
    NSData *datas   = [_data dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *alertData = [NSJSONSerialization JSONObjectWithData:datas
                                                              options:NSJSONReadingMutableContainers
                                                                error:&jsonError];
    
    NSDictionary *textData   = alertData[@"text"];
    NSString *defaultValue   = textData[@"defaultValue"];
    NSString *placeholder    = textData[@"hint"];
    NSString *textType       = textData[@"type"];
    
    
    alertController   = [UIAlertController alertControllerWithTitle:_title message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         
         if ([defaultValue isEqualToString: @""]) {
             
             textField.placeholder = placeholder;
             
             if ([textType isEqualToString:@"numeric"]) {
                 
                 textField.keyboardType = UIKeyboardTypeNumberPad;
                 
             }
             else
             {
                 textField.keyboardType = UIKeyboardTypeAlphabet;
                 
             }
             
         } else {
             
             textField.text = defaultValue;
             enteredText    = defaultValue;
             
         }
         
         [textField addTarget:self
                       action:@selector(alertTextFieldDidChange:)
             forControlEvents:UIControlEventEditingChanged];
         
         
     } ];
    
    
}

- (void)alertTextFieldDidChange:(UITextField *)sender
{
    if (alertController)
    {
        UITextField *login = alertController.textFields.firstObject;
        enteredText = login.text;
        
    }
}

#pragma mark openCheckBoxButtonsDialog

- (void) openCheckBoxButtonsDialog {
    
    NSError *jsonError;
    NSData *datas   = [_data dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *alertData = [NSJSONSerialization JSONObjectWithData:datas
                                                              options:NSJSONReadingMutableContainers
                                                                error:&jsonError];
    
    checkBoxButtonsData = alertData[@"check"];
    buttonLabels = [[NSMutableArray alloc] init];
    int buttonSize = BUTTON_HEIGHT_20;
    CGRect newFrame;
    
    for (NSDictionary *checkBoxButton in checkBoxButtonsData) {
        
        checkBoxButtonId    = checkBoxButton[@"id"];
        checkBoxButtonLabel = checkBoxButton[@"label"];
        checkBoxButtonValue = checkBoxButton[@"selected"];
        
        UIImage *image;
        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(on_click_button:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([checkBoxButtonValue isEqualToString:@"true"]) {
            image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Checkbox.png"]];
            [button setSelected:YES];
        }
        else{
            image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Checkbox-OFF.png"]];
            
        }
        
        UILabel *labelText = [[UILabel alloc] init];
        labelText.text = checkBoxButtonLabel;
        
        CGRect possibleSize = [checkBoxButtonLabel boundingRectWithSize:CGSizeMake(PICKER_WIDTH-60,9999)
                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                             attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                                context:nil];
        
        
        newFrame = labelText.frame;
        newFrame.size.height = possibleSize.size.height;
        newFrame.size.width = possibleSize.size.width;
        labelText.frame = CGRectMake(25, 0, newFrame.size.width, newFrame.size.height);
        
        labelText.lineBreakMode = NSLineBreakByWordWrapping | NSLineBreakByTruncatingTail;
        labelText.textAlignment = NSTextAlignmentLeft; // if you want to
        labelText.numberOfLines = 0;
        [labelText setFont:[UIFont systemFontOfSize:14]];
        labelText.textColor = [UIColor colorWithRed:(90.0/255.0) green:(90.0/255) blue:(90.0/255) alpha:1.0];
        
        labelText.text = checkBoxButtonLabel;
        
        CGFloat imageHeight = image.size.height;
        
        if (newFrame.size.height < imageHeight) {
            
            float newSize = imageHeight - newFrame.size.height;
            newFrame.size.height = newFrame.size.height + newSize;
            button.frame = CGRectMake(10, buttonSize, PICKER_WIDTH-20, newFrame.size.height);
            
        }
        else
        {
            button.frame = CGRectMake(10, buttonSize, PICKER_WIDTH-20 , newFrame.size.height);
            
        }
        
        [button addSubview:labelText];
        
        [button setImage:image forState:UIControlStateNormal];
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        buttonTag    = [[ NSString stringWithFormat: @"%@",checkBoxButtonId] integerValue];
        button.tag   = buttonTag;
        
        [buttonLabels addObject:button];
        
        buttonSize = buttonSize + newFrame.size.height;
    }
    
    UIViewController *controller = [[UIViewController alloc]init];
    UIView *alertView;
    CGRect rect;
    
    rect = CGRectMake(0, -20, PICKER_WIDTH, buttonSize);
    [controller setPreferredContentSize:rect.size];
    alertView  = [[UIView alloc]initWithFrame:rect];
    
    [controller.view addSubview:alertView];
    [controller.view bringSubviewToFront:alertView];
    [controller.view setUserInteractionEnabled:YES];
    [alertView setUserInteractionEnabled:YES];
    
    for (UIButton *btn in buttonLabels) {
        
        [alertView addSubview:btn];
    }
    
    
    alertController = [UIAlertController alertControllerWithTitle:_title message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController setValue:controller forKey:@"contentViewController"];
    
    
}

- (void) on_click_button:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    
    buttonLabel = [sender currentTitle];
    buttonIdString = [NSString stringWithFormat:@"%ld",(long)button.tag];
    
    [button setSelected:!button.isSelected];
    
    if(button.isSelected)
    {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Checkbox.png"]];
        [button setImage:image forState:UIControlStateNormal];
        
        for (NSMutableDictionary *dict3 in checkBoxButtonsData) {
            NSString *strID = dict3[@"id"];
            
            if ([buttonIdString isEqualToString:strID] ) {
                [dict3 setValue:@"true" forKey:@"selected"];
            }
            
        }
        
        
    }
    else
    {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Checkbox-OFF.png"]];
        [button setImage:image forState:UIControlStateNormal];
        
        
        for (NSMutableDictionary *dict3 in checkBoxButtonsData) {
            NSString *strID = dict3[@"id"];
            
            if ([buttonIdString isEqualToString:strID] ) {
                [dict3 setValue:@"false" forKey:@"selected"];
            }
            
        }
        
    }
    
}

#pragma mark openRadioButtonsDialog

- (void) openRadioButtonsDialog {
    
    NSError *jsonError;
    NSData *datas   = [_data dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *alertData = [NSJSONSerialization JSONObjectWithData:datas
                                                              options:NSJSONReadingMutableContainers
                                                                error:&jsonError];
    
    
    radioButtonsData = alertData[@"radio"];
    
    buttonLabels = [[NSMutableArray alloc] init];
    int buttonSize = BUTTON_HEIGHT_20;
    CGRect newFrame;
    
    for (NSDictionary *radioButton in radioButtonsData) {
        
        radioButtonId = radioButton[@"id"];
        radioButtonLabel = radioButton[@"label"];
        radioButtonValue = radioButton[@"selected"];
        
        UIImage *image;
        
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(on_click_button_radio:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([radioButtonValue isEqualToString:@"true"]) {
            image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Radiobutton.png"]];
            [button setSelected:YES];
        }
        else {
            image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Radiobutton-OFF.png"]];
            
        }
        
        button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 3);
        
        UILabel *labelText = [[UILabel alloc] init];
        labelText.text = radioButtonLabel;
        
        CGRect possibleSize = [radioButtonLabel boundingRectWithSize:CGSizeMake(PICKER_WIDTH-60,9999)
                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                          attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                                             context:nil];
        
        
        newFrame = labelText.frame;
        newFrame.size.height = possibleSize.size.height;
        newFrame.size.width = possibleSize.size.width;
        labelText.frame = CGRectMake(25, 0, newFrame.size.width, newFrame.size.height);
        
        labelText.lineBreakMode = NSLineBreakByWordWrapping | NSLineBreakByTruncatingTail;
        // you probably want to center it
        labelText.textAlignment = NSTextAlignmentLeft; // if you want to
        labelText.numberOfLines = 0;
        [labelText setFont:[UIFont systemFontOfSize:14]];
        labelText.textColor = [UIColor colorWithRed:(90.0/255.0) green:(90.0/255) blue:(90.0/255) alpha:1.0];
        
        labelText.text = radioButtonLabel;
        
        CGFloat imageHeight = image.size.height;
        
        
        if (newFrame.size.height < imageHeight) {
            
            float newSize = imageHeight - newFrame.size.height;
            newFrame.size.height = newFrame.size.height + newSize;
            button.frame = CGRectMake(10, buttonSize, PICKER_WIDTH-20, newFrame.size.height);
            
        }
        else
        {
            button.frame = CGRectMake(10, buttonSize, PICKER_WIDTH-20 , newFrame.size.height);
            
        }
        
        
        [button addSubview:labelText];
        
        
        [button setImage:image forState:UIControlStateNormal];
        
        [button setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        
        buttonTag    = [[ NSString stringWithFormat: @"%@",radioButtonId] integerValue];
        button.tag   = buttonTag;
        
        [buttonLabels addObject:button];
        buttonSize = buttonSize + newFrame.size.height;
        
    }
    
    UIViewController *controller = [[UIViewController alloc]init];
    UIView *alertView;
    CGRect rect;
    
    rect = CGRectMake(0, -20, PICKER_WIDTH, buttonSize);
    [controller setPreferredContentSize:rect.size];
    alertView  = [[UIView alloc]initWithFrame:rect];
    
    [controller.view addSubview:alertView];
    [controller.view bringSubviewToFront:alertView];
    [controller.view setUserInteractionEnabled:YES];
    [alertView setUserInteractionEnabled:YES];
    
    for (UIButton *btn in buttonLabels) {
        
        [alertView addSubview:btn];
    }
    
    alertController = [UIAlertController alertControllerWithTitle:_title message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController setValue:controller forKey:@"contentViewController"];
    
    radioButtonLabels = [[NSMutableArray alloc] initWithArray:buttonLabels];
    
}


- (void) on_click_button_radio:(id)sender
{
    
    UIButton *button = (UIButton*)sender;
    buttonLabel = [sender currentTitle];
    buttonIdString = [NSString stringWithFormat:@"%ld",(long)button.tag];
    
    [button setSelected:!button.isSelected];
    
    NSString *radioValue;
    
    for (NSMutableDictionary *radioDict in radioButtonsData) {
        NSString *radioID = radioDict[@"id"];
        radioValue = radioDict[@"selected"];
        
        if ([buttonIdString isEqualToString:radioID] ) {
            [radioDict setValue:@"true" forKey:@"selected"];
        }
        else {
            [radioDict setValue:@"false" forKey:@"selected"];
            
        }
        
    }
    
    for (UIButton *radioButton in radioButtonLabels) {
        
        
        if (radioButton.tag == (long)button.tag) {
            [radioButton setSelected:YES];
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Radiobutton.png"]];
            
            [radioButton setImage:image forState:UIControlStateNormal];
        }
        else {
            [radioButton setSelected:NO];
            
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"OSX-Radiobutton-OFF.png"]];
            [radioButton setImage:image forState:UIControlStateNormal];
            
        }
        
    }
    
}

#pragma mark getResultData

- (NSMutableDictionary *) getResultData {
    
    NSMutableDictionary *resultData = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *resultDictionary = [[NSMutableDictionary alloc] init];
    
    //DatePicker Dialog - date
    if ([_type isEqualToString:typeDate] || [_type isEqualToString:typeTime]  ) {
        
        resultDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys: strDate, typeDate, nil];
        
    }
    
    //DatePicker Dialog - time
    if ([_type isEqualToString:typeTime]  ) {
        
        resultDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys: strDate, typeTime, nil];
        
    }
    
    //Check Dialog
    if ([_type isEqualToString:typeCheck]) {
        
        resultDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys: checkBoxButtonsData, typeCheck, nil];
        
    }
    
    //Radio Buttons Dialog
    if ([_type isEqualToString:typeRadio]) {
        
        resultDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys: radioButtonsData, typeRadio, nil];
        
    }
    
    //Text  Dialog
    if ([_type isEqualToString:typeText]) {
        
        resultDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys: enteredText, typeText, nil];
        
    }
    
    //Info  Dialog
    if ([_type isEqualToString:typeInfo]) {
        
        resultDictionary = [[NSMutableDictionary alloc] initWithObjectsAndKeys: textString, typeInfo, nil];
        
    }
    [resultData setDictionary:resultDictionary];
    // [resultData setValue:resultDictionary forKey:@"type"];
    return resultData;
}

@end
