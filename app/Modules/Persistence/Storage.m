//
//  Storage.m
//  app
//
//  Created by Bernhard Kunnert on 11.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "Storage.h"

@interface Storage ()
{
@private NSString *_fileName;
}

@end

@implementation Storage

+ (Storage*) instance {
    static Storage* _instance = nil;
    if (!_instance) {
        _instance = [[Storage alloc] init];
    }
    
    return _instance;
}

- (id) init {
    self = [super init];
    if (self) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains
        (NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        _fileName = [NSString stringWithFormat:@"%@/storage.bin",
                              documentsDirectory];
        
        // load data
        NSData *storageData = [[NSMutableData alloc]initWithContentsOfFile:_fileName];
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:storageData];
        NSMutableDictionary* dict = [[unarchiver decodeObjectForKey:@"root"] mutableCopy];
        [unarchiver finishDecoding];
        
        if (dict) {
            _dictionary = dict;
        }
    }
    
    return self;
}

- (void) commitChanges {
    NSMutableData* storageData = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:storageData];
    [archiver encodeObject:_dictionary forKey: @"root"];
    [archiver finishEncoding];
    [storageData writeToFile:_fileName atomically:YES];
}

- (id) removeValue {
    NSLog(@"_dictionaryStorageDataFull %@",_dictionary );

    [_dictionary removeAllObjects];
    _dictionary = [[NSMutableDictionary alloc] init];
    NSLog(@"_dictionaryStorageDataDell %@",_dictionary );

    return _dictionary;
}


@end
