//
//  CalendarEvents.h
//  app
//
//  Created by LeyLa Mehmed on 1/28/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>

@interface CalendarEvents : NSObject

typedef void (^LoginHandler)(NSDictionary* resultDictionary);


@property (strong, nonatomic) NSString *savedEventId;
@property (strong, nonatomic) NSMutableArray *allCalendars;
@property (strong, nonatomic) NSMutableArray *eventIds;
@property (strong, nonatomic) EKEventStore *eventStore;
@property (strong, nonatomic) NSDictionary *resultDictionary;

@property  EKCalendar *defaultCalendar;

+ (CalendarEvents *) sharedInstance;
- (instancetype) init;

- (NSMutableArray*) saveToCalendar:(NSString*) calendarIdsForEvents title: (NSString*) title startDate:(NSDate*) startDate endDate:(NSDate*) endDate notes: (NSString*) notes reminder:(NSString*) reminder location:(NSString*) location;

- (BOOL) updateEvent:(NSString*) event_id title: (NSString*) title startDate:(NSDate*) startDate endDate:(NSDate*) endDate notes: (NSString*) notes reminder:(NSString*) reminder location:(NSString*) location;
- (BOOL) deleteEvent:(NSString*) event_id;

- (void) getCalendars;
//- (void) getAllCalendars;
- (NSMutableDictionary *) getArray;
- (BOOL) getAllCalendars: (LoginHandler) handler;


@end
