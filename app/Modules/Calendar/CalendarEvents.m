//
//  CalendarEvents.m
//  app
//
//  Created by LeyLa Mehmed on 1/28/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CalendarEvents.h"

@implementation CalendarEvents

+ (CalendarEvents *)sharedInstance {
    static dispatch_once_t onceToken;
    static CalendarEvents *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[CalendarEvents alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (EKEventStore *)eventStore {
    if (!_eventStore) {
        _eventStore = [[EKEventStore alloc] init];
    }
    return _eventStore;
}
- (BOOL) getAllCalendars: (LoginHandler) handler {
    BOOL result = NO;

    NSString *version = [[UIDevice currentDevice] systemVersion];
    if ([version floatValue] >= 6.0f) {
        // iOS6
        EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
        
        if (status == EKAuthorizationStatusNotDetermined) {

            EKEventStore *eventStore = [[EKEventStore alloc] init];
            [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                if (granted) {
                    [self getCalendars];
                    self.resultDictionary = [self getArray];
                    if (handler) {
                        handler(self.resultDictionary);
                    }
                } else {

                }
            }];
        }
        else if (status == EKAuthorizationStatusDenied || status == EKAuthorizationStatusRestricted) {
            // EKAuthorizationStatusRestricted - iPhone
            // EKAuthorizationStatusDenied -
            return result;
        }
        else if (status == EKAuthorizationStatusAuthorized) {
            
            [self getCalendars];
            self.resultDictionary = [self getArray];
            if (handler) {
                handler(self.resultDictionary);
            }
            
        }
    } else {
        [self getCalendars];
        self.resultDictionary = [self getArray];
        if (handler) {
            handler(self.resultDictionary);
        }

    }
    
    return YES;
}

- (void) getCalendars {
    
    EKEntityType type = EKEntityTypeEvent;
    
    NSArray *calendars = [self.eventStore calendarsForEntityType:type];
    
    _allCalendars = [[NSMutableArray alloc] init];
    
    for (EKCalendar *calendar in calendars) {
        
        if (calendar.allowsContentModifications != NO) {
            NSDictionary *calendarsDict = [[NSDictionary alloc] initWithObjectsAndKeys:calendar.calendarIdentifier, @"calendar_id", calendar.title ,@"display_name", nil];
            
            [_allCalendars addObject:calendarsDict];
        }
    }
}

- (NSMutableDictionary *) getArray {
    
NSMutableDictionary *d = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_allCalendars,@"allCalendras", nil];
    
    return d;
}

- (NSMutableArray *) saveToCalendar:(NSString*) calendarIdsForEvents title: (NSString*) title startDate:(NSDate*) startDate endDate:(NSDate*) endDate notes: (NSString*) notes reminder:(NSString*) reminder location:(NSString*) location
{
    EKEventStore* store = [[EKEventStore alloc] init];
    NSArray *calendarIds = [calendarIdsForEvents componentsSeparatedByString:@","];
    _eventIds =[[NSMutableArray alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
    
    NSString *stringStartDate = [formatter stringFromDate:startDate];
    NSString *stringEndDate = [formatter stringFromDate:endDate];
    
    if ((![calendarIdsForEvents isEqualToString:@""]) && (![title isEqualToString:@""]) && (![stringStartDate isEqualToString:@"1970"]) &&  (![stringEndDate isEqualToString:@"1970"]) ) {
        
        for (NSString *calendarId in calendarIds) {
            
            EKEvent* newEvent      = [EKEvent eventWithEventStore:store];
            newEvent.title         = title;
            newEvent.startDate     = startDate;
            newEvent.endDate       = endDate;
            newEvent.notes         = notes;
            newEvent.location      = location;
            

            //Set reminder if reminder is not empty string
            
            if (![reminder isEqual:@""]) {
                
                NSMutableArray *alarms = [[NSMutableArray alloc] init];
                EKAlarm *alarm         = [EKAlarm alarmWithRelativeOffset:[reminder integerValue]];
                [alarms addObject:alarm];
                newEvent.alarms = alarms;

            }

            EKEntityType type      = EKEntityTypeEvent;
            NSArray *calendars     = [store calendarsForEntityType:type];
            
            for (EKCalendar *calendar in calendars) {
                
                if ([calendar.calendarIdentifier  isEqual:calendarId] ) {
                    
                    newEvent.calendar = calendar;
                    [store saveEvent:newEvent span:EKSpanThisEvent commit:YES error:nil];
                    
                }
                
            }
            
            _savedEventId = [NSString stringWithFormat:@"%@", newEvent.eventIdentifier];
            
            NSDictionary* personDict = [[NSDictionary alloc] initWithObjectsAndKeys:_savedEventId, @"event_id", calendarId, @"calendar_id", nil];
            
            [_eventIds addObject:personDict];
            
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save event" message:@"Success" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"CalendarIds, Title, Start date and End date fields are required" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    }
    return _eventIds;
    
}
- (BOOL) updateEvent:(NSString*) event_id title: (NSString*) title startDate:(NSDate*) startDate endDate:(NSDate*) endDate notes: (NSString*) notes reminder:(NSString*) reminder location:(NSString*) location {
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) return;
        EKEvent *event = [store eventWithIdentifier:event_id];
        if (event) {
            NSError *err = nil;
            
            event.title         = title;
            event.startDate     = startDate;
            event.endDate       = endDate;
            event.notes         = notes;
            event.location      = location;
            
            //Set reminder if reminder is not empty string
            
            if (![reminder isEqual:@""]) {
                
                NSMutableArray *alarms = [[NSMutableArray alloc] init];
                EKAlarm *alarm         = [EKAlarm alarmWithRelativeOffset:[reminder integerValue]];
                
                [alarms addObject:alarm];
                event.alarms = alarms;
                
            }
            
            [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        }
    }];
    
    return YES;
}

-(BOOL) deleteEvent:(NSString*) event_id {
    
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) return;
        EKEvent* eventToRemove = [store eventWithIdentifier:event_id];
        if (eventToRemove) {
            NSError* err = nil;
            [store removeEvent:eventToRemove span:EKSpanThisEvent commit:YES error:&err];
        }
    }];
    
    return YES;
}


@end
