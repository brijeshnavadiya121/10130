//
//  CFSlidingTabItem.m
//  app
//
//  Created by LeyLa on 4/30/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import "CFSlidingTabItem.h"
#import "CFPage+JSON.h"
#import "UIColor+Additions.h"
#import "UIImage+Asset.h"

@interface CFSlidingTabItem ()

@property (strong, nonatomic) CFPage* cfPage;

@end

@implementation CFSlidingTabItem

#pragma mark BSJsonSupported

- (id)proxyForJson
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];

    [dict setObject:self.title forKey:@"title"];
    if (self.iconName) {
        [dict setObject:self.iconName forKey:@"icon"];
    }

    return dict;
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    if ([super init]) {
        if (!receivedObjects) {
            return nil;
        }

        receivedObjects = [CFPage removeNullObjectsFromDictionary:receivedObjects];

        self.title = receivedObjects[@"title"];
        self.iconName = receivedObjects[@"icon"];
        self.icon = [UIImage imageFromAsset:self.iconName];
    }

    return self;
}


- (id) initWithPage:(CFPage *)page {
    self = [super init];
    if (self) {
        self.cfPage = page;
    }

    return self;
}


@end
