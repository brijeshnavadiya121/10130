//
//  SlidingTabViewController.h
//  app
//
//  Created by LeyLa on 5/3/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import "BaseSlidingTabsViewController.h"
#import "Navigation.h"
#import "SLPagingViewController.h"

@interface SlidingTabViewController : SLPagingViewController <Navigation>

@property (strong, nonatomic) UIWindow *window;
@end
