//
//  SlidingTabViewController.m
//  app
//
//  Created by LeyLa on 5/3/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import "SlidingTabViewController.h"
#import "SLPagingViewController.h"
#import "UIColor+SLAddition.h"
#import "CFSlidingTabPage.h"
#import "CFSlidingTabItem.h"
#import "DefaultNavigationImpl.h"
#import "NavigationViewController.h"
#import "CFRuntime.h"
#import "UIViewController+Container.h"
#import "NativeBridge.h"
#import "WebviewViewController.h"
#import "CFHtmlPage.h"
#import "SlidingTabsHelper.h"
#import "AppDelegate.h"
@interface SlidingTabViewController () <NavigationService, NavigationResultDelegate >
{
    BOOL _initialized;
}
@property (strong, nonatomic) UINavigationController *nav;
@property (strong, nonatomic) DefaultNavigationImpl* navigationImpl;
@end

@implementation SlidingTabViewController

@synthesize navigationContext = _navigationContext;
@synthesize page = _page;
@synthesize delegate = _delegate;
@synthesize typedPage = _typedPage;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self->_initialized = NO;
    }
    return self;
}

- (void) setNavigationContext:(id)navigationContext {
    for (id container in self.viewControllers) {
        [[container viewControllers][0] setNavigationContext:self.navigationContext];
    }
}

- (void) setPage:(CFPage *)page
{
    BOOL reloadSubPages = !_page;

    if (_page != page) {
        _page = page;
        if (_page && reloadSubPages) {
            // initial loading of page
            [self updateAppearance];
        }
    }
}


- (void) updateAppearance
{
  //  [SlidingTabsHelper sharedInstance].navContext = @"empty";
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    CFSlidingTabPage* slidingTabPage = (CFSlidingTabPage*) self.typedPage;
    if (!slidingTabPage) {
        slidingTabPage = [[CFSlidingTabPage alloc] initWithPage:_page];
        self.typedPage = slidingTabPage;
    }

    if (!self->_initialized) {

            //  CFSlidingTabPage *slidingTabPage =[[CFSlidingTabPage alloc] initWithPage:_page];


        //Get navbar background
        NSString *navBarBackroundString = slidingTabPage.navbarBackgroudColor;
        UIColor *navBarBackround  = [UIColor colorFromHexString:navBarBackroundString];

        //Get tabItems
        NSArray *tabItems =  slidingTabPage.tabItems;

        NSMutableArray *icons =  [[NSMutableArray alloc] init];
        NSMutableArray *viewsArray =  [[NSMutableArray alloc] init];

        //Array with icons and views
        NSMutableArray* viewControllers = [NSMutableArray arrayWithCapacity:tabItems.count];

            for (id tabItem in tabItems) {
         //       UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTitle:[tabItem title] image:[((CFSlidingTabItem*) tabItem) icon] tag:0];
                UIViewController<Navigation>* viewController = [[CFRuntime instance].viewControllerFactory viewControllerForPage:[tabItem cfPage]];

                viewController.navigationService.page = [tabItem cfPage];
                viewController.navigationService.navigationContext = self.navigationContext;
                viewController.navigationService.delegate = self;
                viewController.hidesBottomBarWhenPushed = NO;

                BaseNavigationController* container = [[BaseNavigationController alloc] initWithRootViewController:viewController];

               // container.tabBarItem = tabBarItem;
                [viewControllers addObject:container];

                //Array with views
                UIView *v = [UIView new];
                v = viewController.view;
                [viewsArray addObject:v];

                UIImage *img;
                NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
                NSString *imagePath =  [directoryPath objectAtIndex:0];
                imagePath= [imagePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/content/%@",[((CFSlidingTabItem*) tabItem) iconName]]];

                img = [UIImage imageWithContentsOfFile:imagePath];
                img = [img imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                //Array with icons
                [icons addObject: [[UIImageView alloc] initWithImage:img]];


            }
        NSMutableDictionary * dic = [NSMutableDictionary dictionary];
        [dic setObject:viewControllers forKey:@"dic"];

        [self setViewControllers:dic];

        //Get icon colors / selected icon colors - default value for icon - GRAY, selected icon - ORANGE(RED)
        //Get iconColor background

        UIColor *orange;
        UIColor *gray;

        NSString *iconColorString = slidingTabPage.iconColor;

        if (iconColorString) {
            UIColor *iconColor  = [UIColor colorFromHexString:iconColorString];
            gray = iconColor;
        } else {
            gray = [UIColor colorWithRed:.84
                                   green:.84
                                    blue:.84
                                   alpha:1.0];
        }

        //Get iconColorSelected background
        NSString *iconColorSelectedStr = slidingTabPage.iconColorSelected;

        if (iconColorSelectedStr) {
            UIColor *iconColorSelected  = [UIColor colorFromHexString:iconColorSelectedStr];
            orange = iconColorSelected;
        } else {
            orange = [UIColor colorWithRed:255/255
                                     green:69.0/255
                                      blue:0.0/255
                                     alpha:1.0];
        }

      //  SLPagingViewController *pageViewController = [[SLPagingViewController alloc] initWithNavBarItems:icons navBarBackground:navBarBackround views:viewsArray showPageControl:NO];

       // SLPagingViewController *pageViewController = [[SLPagingViewController alloc] initWithNavBarItems:icons controllers:viewControllers showPageControl:NO];
        SLPagingViewController *pageViewController = [[SLPagingViewController alloc] initWithNavBarItems:icons navBarBackground:navBarBackround controllers:viewControllers showPageControl:NO];
        pageViewController.navigationSideItemsStyle = SLNavigationSideItemsStyleOnBounds;
        float minX = 45.0;
        // Tinder Like
        pageViewController.pagingViewMoving = ^(NSArray *subviews){
            float mid  = [UIScreen mainScreen].bounds.size.width/2 - minX;
            float midM = [UIScreen mainScreen].bounds.size.width - minX;
            for(UIImageView *v in subviews){
                UIColor *c = gray;
                if(v.frame.origin.x > minX
                   && v.frame.origin.x < mid)
                    // Left part
                    c = [UIColor gradient:v.frame.origin.x
                                      top:minX+1
                                   bottom:mid-1
                                     init:orange
                                     goal:gray];
                else if(v.frame.origin.x > mid
                        && v.frame.origin.x < midM)
                    // Right part
                    c = [UIColor gradient:v.frame.origin.x
                                      top:mid+1
                                   bottom:midM-1
                                     init:gray
                                     goal:orange];
                else if(v.frame.origin.x == mid)
                    c = orange;
                v.tintColor= c;
            }
        };
        self->_initialized = YES;
        [SlidingTabsHelper sharedInstance].isEnable = YES;
        [SlidingTabsHelper sharedInstance].slidingTabs = YES;

        [SlidingTabsHelper sharedInstance].nav = [[BaseNavigationController alloc] initWithRootViewController:pageViewController];
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.window setRootViewController: [SlidingTabsHelper sharedInstance].nav];
        [appDelegate.window makeKeyAndVisible];

    }else{

        NSArray* viewControllers = [NSArray arrayWithObjects:[self viewControllers], nil];
        for (int i = 0; i < viewControllers.count; i++) {
            if (slidingTabPage.tabItems.count > i) {
                // CFSlidingTabItem* tabItem = slidingTabPage.tabItems[i];
               // UIViewController* viewController = viewControllers[i];
               // Sl
               // UITabBarItem* tabBarItem = [[UITabBarItem alloc] initWithTitle:[tabItem title] image:[tabItem icon] tag:0];
               // viewController.tabBarItem = tabBarItem;

            }
    }

}
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) isContainer
{
    return YES;
}

#pragma mark Navigation

- (id<NavigationService>) navigationService {
    return self;
}

- (void) navigateToPage:(CFPage *)page withContext:(id)navigationContext {
    //invalid, navigation is only allowed from one of the root pages.
}
- (void) navigateToPageAndAddToStack:(CFPage *)page withContext:(id)navigationContext {
    //invalid, navigation is only allowed from one of the root pages.
}

- (void) navigateBackWithResult:(id)result {
    // invalid, navigation is only allowed fromone of the root pages.
}
- (void) navigateBackWithResult:(id)result onStackView:(NSString *) pageId {
    // invalid, navigation is only allowed fromone of the root pages.
}
- (void) navigateBackWithResultToRoot:(id)result onStackView:(NSString *) pageId{

}
- (void) navigateToRoot {
    // invalid, navigation is only allowed fromone of the root pages.
}


- (void) dismissNavigation:(id<Navigation>)navigation withResult:(id)result {
    // dismiss self
    if (self.delegate) {
        [self.delegate dismissNavigation:self withResult:result];
    }
}
- (void) dismissNavigationOnStack:(id<Navigation>)navigation withResult:(id)result andPageId: (NSString *) pageId {
    // dismiss self
    if (self.delegate) {
        [self.delegate dismissNavigationOnStack:self withResult:result andPageId:pageId];
    }
}
- (void) dismissNavigationToRoot:(id<Navigation>)navigation withResult:(id)result andPageId: (NSString *) pageId {
    // dismiss self
    if (self.delegate) {
        [self.delegate dismissNavigationOnStack:self withResult:result andPageId:pageId];
    }
}

- (void) applyHeaderAttributesOnNavigationController:(UINavigationController *)navigationController andButtonTarget:(id<ButtonTarget>)target
{
    [DefaultNavigationImpl clearPageHeaderAttributes:navigationController];

    BOOL shouldApplyOwnHeader = YES;

    // Do not apply own header if a subview has hidden the tab bar for navigating to one of their "children".
    if ([self.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* container = (UINavigationController*) self.presentedViewController;

        shouldApplyOwnHeader = container.viewControllers.count <= 1;
    }

    if (shouldApplyOwnHeader) {
        [DefaultNavigationImpl applyPageHeaderAttributes:self.navigationService.page.pageHeader toNavigationController:navigationController andButtonTarget:target];
    }
}

@end
