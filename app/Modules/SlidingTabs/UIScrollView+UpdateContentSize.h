//
//  UIScrollView+UpdateContentSize.h
//  app
//
//  Created by LeyLa on 5/3/18.
//  Copyright © 2018 CloudFaces. All rights reserved.
//

#import "ShareResources.h"

@interface UIScrollView (UpdateContentSize)

-(void)updateContentSize;

@end
