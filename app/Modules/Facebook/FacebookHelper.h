//
//  FacebookHelper.h
//  app
//
//  Created by LeyLa Mehmed on 2/25/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef void (^LoginHandler)(NSDictionary* resultDictionary);

@interface FacebookHelper : NSObject

@property (nonatomic, readonly) NSString *accessToken;
@property (nonatomic, strong) NSDictionary *resultDictionary;


+ (instancetype) instance;
- (BOOL) login: (LoginHandler) handler  permissions: (NSString *) permissions;
- (void) logout;

- (void) appInvates: (NSString *) appLinkString andPreviewUrl: (NSString *) string;

@end
