//
//  FacebookHelper.m
//  app
//
//  Created by LeyLa Mehmed on 2/25/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "FacebookHelper.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface FacebookHelper ()

@property (nonatomic, strong) NSString* accessToken;
@property (nonatomic, strong) NSString* permissions;

@end

@implementation FacebookHelper


+ (instancetype) instance {
    static FacebookHelper *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (instancetype) init {
    self = [super init];
    if (self) {
        if ([FBSDKAccessToken currentAccessToken]) {
            
            NSLog(@"TOKEN %@", [FBSDKAccessToken currentAccessToken].tokenString);
            _accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
        }
    }
    return self;
}


- (BOOL) login:(LoginHandler) handler permissions: (NSString *) permissions {
    
    _permissions = permissions;
    BOOL result = NO;
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
   // login.loginBehavior = FBSDKLoginBehaviorNative;
    
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                           parameters:@{@"fields": @"picture, email"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error) {
                 
                 _resultDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"success", @"status",_accessToken, @"token", nil];
                 
                 if (handler) {
                     handler(self.resultDictionary);
                 }
                 
             }
             else{
                 
                 _resultDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"error", @"status", [error localizedDescription], @"message", nil];
                 
                 if (handler) {
                     handler(self.resultDictionary);
                 }
                 
             }
             
         }];
        
        
    } else {
        NSArray *permsArray = [_permissions componentsSeparatedByString:@","];
        
        [login logInWithPermissions:permsArray fromViewController:self handler:^(FBSDKLoginManagerLoginResult *results, NSError *error) {
                                    if (error) {
                                        
                                        _resultDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"error", @"status",[error localizedDescription], @"message", nil];
                                        
                                        if (handler) {
                                            handler(self.resultDictionary);
                                        }
                                    } else if (results.isCancelled) {
                                        
                                        _resultDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"cancel", @"status", nil];
                                        
                                        if (handler) {
                                            handler(self.resultDictionary);
                                        }
                                    } else {
                                        
                                        _accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
                                        
                                        _resultDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"success", @"status",_accessToken, @"token", nil];
                                        if (handler) {
                                            handler(self.resultDictionary);
                                        }
                                    }
                                }];
        
        return result;
        
    }
    return  YES;
}



- (void) appInvates: (NSString *) appLinkString andPreviewUrl: (NSString *) string{

  //  [FBSDKAppInviteDialog showFromViewController:self
//                                     withContent:content
//                                        delegate:self];


}


-(void) logout {
    
    [[[FBSDKLoginManager alloc] init] logOut];
    [FBSDKAccessToken setCurrentAccessToken:nil];
    _accessToken = nil;
    
}


@end
