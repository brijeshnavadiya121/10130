//
//  CFRuntime.h
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "CFViewControllerFactory.h"
#import <DDXML.h>
#import "Reachability.h"

@interface CFRuntime : NSObject {
    NSMutableArray* rightButtons;
}

@property (strong, nonatomic, readonly) CFViewControllerFactory* viewControllerFactory;
@property (strong, nonatomic, readonly) NSString* baseUrl;
@property (strong, nonatomic) NSMutableArray* rightButtons;
@property (strong, nonatomic) CFContentView* contentViewButton;
@property (strong, nonatomic) CFHeaderButton* leftButton;
@property (strong, nonatomic) NSMutableDictionary* webViewStack;
@property (strong, nonatomic) NSString* overlayErrorLabel;
@property (strong, nonatomic) NSString* overlayWarnLabel;

@property (strong, nonatomic) NSString* orientation;


+ (CFRuntime*) instance;
+ (NSString*) pathForAppFile:(NSString*) file ofType:(NSString*) type inDirectory:(NSString*) directory;
+ (NSString*) pathForExtractedAppContent;
+ (NSString*) pathForExtractedRemoteAppContent;
+ (NSString*) pathForExtractedLocalAppContent;
+ (NSString*) pathForExtractedApp;

+ (void) presentPage:(CFPage*) page; // present a page modally
+ (void) presentPage:(CFPage*) page withDebugoverlayInPosition:(NSString *) position; // present a page modally with debug overlay in position (top left, top right, bottom left, bottom right)

- (BOOL) handleAppUpgrade;
- (BOOL) resetAppFiles;
- (BOOL) loadAppFromUrl:(NSURL*) url;
- (CFPage*) getRootPage;
- (CFPage*) getPageWithId:(NSString*) pageId;
- (DDXMLElement*) getPageElement:(NSString*) pageId;
- (NSArray*) getLocalFilePaths;
- (BOOL) downloadAndReplaceLocalFile:(NSString*) localPath withContentOfUrl:(NSURL*) remoteUrl;
- (NSString *) getConnectionType;
- (void) changeErrorLabel: (NSString *) errors;
- (void) changeWarnLabel: (NSString *) warnings;
- (NSString*) getRemoteContent;
@end
