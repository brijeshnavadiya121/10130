//
//  CFHttpResponseHelper.m
//  app
//
//  Created by LeyLa on 8/26/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//
#import "CFHttpResponseHelper.h"

@implementation CFHttpResponseHelper

@synthesize delegate;

+ (CFHttpResponseHelper *)sharedInstance {
    static dispatch_once_t onceToken;
    static CFHttpResponseHelper *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[CFHttpResponseHelper alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)setResponse: (NSString *) response {
    if (self.delegate) {
        [self.delegate getResponseFromPositionUpdate:response];
    }
}

- (void) sethandleNotification:(NSDictionary *) response {
    NSLog(@"sethandleNotification response %@", response);
    if (self.delegate) {
        [self.delegate push:response];
    }
}

@end
