//
//  CFHttpRequest.h
//  app
//
//  Created by Bernhard Kunnert on 04.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CFHttpResponse.h"

@class CFHttpRequest;
@protocol CFHttpRequestDelegate <NSObject>

- (void) requestCompleted:(CFHttpRequest*) request;
//+ (BOOL)canInitWithRequest:(NSURLRequest *)request;

@end

@interface CFHttpRequest : NSObject

@property(nonatomic, copy) NSString* url;
@property(nonatomic, copy) NSString* method;
@property(nonatomic, copy) NSString* body;
@property(nonatomic, copy) NSString* filePath;
@property(nonatomic, strong) NSDictionary* headers;
@property(nonatomic, assign) NSInteger callbackId;
@property(nonatomic, weak) id<CFHttpRequestDelegate> delegate;
@property(nonatomic, readonly) CFHttpResponse* response;
//s@property(nonatomic, strong) NSString* appHash;


- (void) send;
- (void) sendFile;

@end
