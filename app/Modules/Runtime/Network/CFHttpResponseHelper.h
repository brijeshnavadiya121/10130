//
//  CFHttpResponseHelper.h
//  app
//
//  Created by LeyLa on 8/26/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>

@protocol CFHttpResponseHelperDelegate <NSObject>

- (void) getResponseFromPositionUpdate: (NSString *) response;
- (void) push: (NSDictionary *) response;

@end

@interface CFHttpResponseHelper : NSObject  <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>


@property (weak, nonatomic)  id<CFHttpResponseHelperDelegate> delegate;
//@property (strong, nonatomic) NSString *appHash;
+ (CFHttpResponseHelper *)sharedInstance;
- (instancetype) init;

- (void) setResponse: (NSString *) response;
- (void) sethandleNotification: (NSDictionary *) response;

@end
