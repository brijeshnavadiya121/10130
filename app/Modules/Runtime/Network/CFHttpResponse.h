//
//  CFHttpResponse.h
//  app
//
//  Created by Bernhard Kunnert on 04.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CFHttpResponse : NSObject

+ (CFHttpResponse *)sharedInstance;
- (instancetype) init;
-(NSString *) getResponse;

@property(nonatomic, assign) NSInteger code;
@property(nonatomic, copy) NSString* body;

@end
