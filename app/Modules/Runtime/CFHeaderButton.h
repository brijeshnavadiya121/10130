//
//  CFHeaderButton.h
//  app
//
//  Created by Bernhard Kunnert on 26.02.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSJsonSupported.h"

@interface CFHeaderButton : NSObject <BSJsonSupported>

@property(nonatomic, copy) NSString* text;
@property(nonatomic, strong) UIColor* textColor;
@property(nonatomic, strong) UIImage* icon;
@property(nonatomic, copy) NSString* iconName;
@property(nonatomic, copy) NSString* function;

@end
