//
//  CFSliderPage.m
//  app
//
//  Created by Bernhard Kunnert on 05.03.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "CFSliderPage.h"
#import "CFRuntime.h"
#import "MenuPositionHelper.h"
#import "SlidingTabsHelper.h"

@interface CFSliderPage ()

@property (nonatomic, strong, readwrite) CFPage* page;

@end

@implementation CFSliderPage

@dynamic menuPage;
@dynamic contentPage;

- (id)proxyForJson
{
    return [self.page proxyForJson];
}

- (instancetype) initWithJson:(NSDictionary*) receivedObjects
{
    self = [self initWithPage:[[CFPage alloc] initWithJson:receivedObjects]];
    return self;
}

- (id) initWithPage:(CFPage *)page {
    self = [super init];
    if (self) {
        self.page = page;
    }
    return self;
}

- (CFPage*) contentPage
{
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
    NSError* error;
    NSArray* elements = [pageElement nodesForXPath:@"./drawer/@mainpage" error:&error];

    CFPage *mainPage;

   // Needed for Sliding Tabs NavigateToRoot(Page_id)
    if ([[[SlidingTabsHelper sharedInstance] checkIsEnable] isEqual:@"YES"]) {
        [SlidingTabsHelper sharedInstance].slidingTabs = NO;
        mainPage = [[CFRuntime instance] getPageWithId: [SlidingTabsHelper sharedInstance].pageId];
    }else{
        mainPage = [[CFRuntime instance] getPageWithId:[elements[0] stringValue]];
    }
    return mainPage;
}

- (CFPage*) menuPage
{
    DDXMLElement* pageElement = [[CFRuntime instance] getPageElement:self.page.uniqueId];
    NSError* error;
    NSArray* elements = [pageElement nodesForXPath:@"./drawer/@drawerpage" error:&error];

    if ([pageElement nodesForXPath:@"./drawer/@slide" error:&error]) {
        NSArray* slide = [pageElement nodesForXPath:@"./drawer/@slide" error:&error];
        if (slide!= nil && [slide count] != 0) {
            if (slide && ![slide isEqual:@""]) {
                _slide = [slide[0] stringValue];
            }
        }else{
            _slide = @"true";
        }
    }
    
    if ([pageElement nodesForXPath:@"./drawer/@tap" error:&error]) {
        NSArray* tap = [pageElement nodesForXPath:@"./drawer/@tap" error:&error];
        if (tap!= nil && [tap count] != 0) {
            if (tap && ![tap isEqual:@""]) {
                _tap = [tap[0] stringValue];
            }
        }else{
            _tap = @"true";
            
        }
    }

    //rigt or left
    if ([pageElement nodesForXPath:@"./drawer/@direction" error:&error]) {
        NSArray* direction = [pageElement nodesForXPath:@"./drawer/@direction" error:&error];
        if (direction!= nil && [direction count] != 0) {
            if (direction && ![direction isEqual:@""]) {

                _direction = [direction[0] stringValue];
            }
        } else {
            _direction = @"left";
        }
    }

    return [[CFRuntime instance] getPageWithId:[elements[0] stringValue]];
}

-(NSString *) getSlideString {
    NSString *slideString = [[NSString alloc] initWithString:_slide];
    return slideString;
}

@end
