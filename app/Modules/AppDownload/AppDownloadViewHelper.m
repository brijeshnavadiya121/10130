//
//  AppDownloadViewHelper.m
//  app
//
//  Created by LeyLa on 5/12/17.
//  Copyright © 2017 CloudFaces. All rights reserved.
//

#import "AppDownloadViewHelper.h"
#import "AppDelegate.h"
#import "CFRuntime.h"
#import "SlidingViewController.h"
#import "WebviewViewController.h"
#import <UIKit/UIKit.h>


#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)
#define iOS7_0 @"7.0"
@implementation AppDownloadViewHelper
@synthesize delegate;

BOOL ButtonSelected;

UIView  * logView;
NSArray * logArray;


int cellCount;

+ (AppDownloadViewHelper *)sharedInstance {
    static dispatch_once_t onceToken;
    static AppDownloadViewHelper *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[AppDownloadViewHelper alloc] init];

    });

    return instance;
}

- (id)init {
    self = [super init];
    if (self) {

        self.errorArray = [[NSMutableArray alloc] init];
        self.warnArray  = [[NSMutableArray alloc] init];
    }
    return self;
}

- (NSArray *) getLogArray {

    return self.logArray;
}

- (void) openLogView {
    self.cpArr  = [[NSMutableArray alloc] init];

    _showTimes = @"false";
    
    UIView *currentVc =  [[[[UIApplication sharedApplication] keyWindow] subviews] lastObject];

    logView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,  currentVc.frame.size.width, currentVc.frame.size.height)];
    logView.backgroundColor = [UIColor whiteColor];

    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, currentVc.frame.size.width, 100)];

    NSMutableArray *customButtons =  [[NSMutableArray alloc] init];

   UIColor *ios7BlueColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    UIButton* clearLogbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearLogbutton setTitle:NSLocalizedString(@"Clear\nlog", nil) forState:UIControlStateNormal];
    [clearLogbutton addTarget:self
            action:@selector(clearLogPressed)
             forControlEvents:UIControlEventTouchUpInside];

    UIButton* reloadAppbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [reloadAppbutton setTitle:NSLocalizedString(@"Reload\napp", nil) forState:UIControlStateNormal];
    [reloadAppbutton addTarget:self
                       action:@selector(reloadAppPressed)
             forControlEvents:UIControlEventTouchUpInside];

    UIButton* stopAppbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [stopAppbutton setTitle:NSLocalizedString(@"Stop\napp", nil) forState:UIControlStateNormal];
    [stopAppbutton addTarget:self
                        action:@selector(stopAppPressed)
              forControlEvents:UIControlEventTouchUpInside];

    self.showTimesbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.showTimesbutton setTitle:NSLocalizedString(@"Show\ntimes", nil) forState:UIControlStateNormal];
    [self.showTimesbutton addTarget:self
                      action:@selector(showTimePressed)
            forControlEvents:UIControlEventTouchUpInside];

    UIButton* clearDatabutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearDatabutton setTitle:NSLocalizedString(@"Clear\ndata", nil) forState:UIControlStateNormal];
    [clearDatabutton addTarget:self
                      action:@selector(clearDataPressed)
            forControlEvents:UIControlEventTouchUpInside];

    UIButton* copyLogbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [copyLogbutton setTitle:NSLocalizedString(@"Copy\nlog", nil) forState:UIControlStateNormal];
    [copyLogbutton addTarget:self
                      action:@selector(copyLogPressed)
            forControlEvents:UIControlEventTouchUpInside];

    UIButton* closeLogViewbutton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeLogViewbutton setTitle:NSLocalizedString(@"X", nil) forState:UIControlStateNormal];

    [closeLogViewbutton addTarget:self
                           action:@selector(handleExit)
                 forControlEvents:UIControlEventTouchUpInside];

    UIButton* emptyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [emptyBtn setTitle:NSLocalizedString(@" ", nil) forState:UIControlStateNormal];
    CGRect buttonFrame2 = emptyBtn.frame;
    buttonFrame2.size = CGSizeMake(1, 20);
    emptyBtn.frame = buttonFrame2;

    [customButtons addObject:clearLogbutton];
    [customButtons addObject:reloadAppbutton];
    [customButtons addObject:stopAppbutton];
    [customButtons addObject:self.showTimesbutton];
    [customButtons addObject:clearDatabutton];
    [customButtons addObject:copyLogbutton];
    [customButtons addObject:closeLogViewbutton];
    [customButtons addObject:clearLogbutton];


    for (UIButton *btn in customButtons) {
        btn.titleLabel.numberOfLines = 2;

        CGRect buttonFrame = closeLogViewbutton.frame;
        buttonFrame.size = CGSizeMake(38, 40);

        btn.frame = buttonFrame;

        btn.titleLabel.font = [UIFont systemFontOfSize: 10];
        [btn.titleLabel setTextAlignment: NSTextAlignmentCenter];
        [btn setTitleColor:ios7BlueColor forState:UIControlStateNormal];
        btn.layer.borderWidth=1.0f;
        btn.layer.cornerRadius = 7;
        btn.layer.borderColor=ios7BlueColor.CGColor;

        
    }

    closeLogViewbutton.titleLabel.font = [UIFont systemFontOfSize: 20];


    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -10;
    UIBarButtonItem *emptyBtn1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    emptyBtn1.width = 7;
    UIBarButtonItem *clearLog = [[UIBarButtonItem alloc] initWithCustomView:clearLogbutton];
    UIBarButtonItem *reloadApp = [[UIBarButtonItem alloc] initWithCustomView:reloadAppbutton];
    UIBarButtonItem *stopApp = [[UIBarButtonItem alloc] initWithCustomView:stopAppbutton];
    UIBarButtonItem *showTimes = [[UIBarButtonItem alloc] initWithCustomView:self.showTimesbutton];
    UIBarButtonItem *clearData = [[UIBarButtonItem alloc] initWithCustomView:clearDatabutton];
    UIBarButtonItem *copyLog = [[UIBarButtonItem alloc] initWithCustomView:copyLogbutton];
    UIBarButtonItem *closeLogView = [[UIBarButtonItem alloc] initWithCustomView:closeLogViewbutton];


    [toolbar setItems:[[NSArray alloc] initWithObjects:emptyBtn1, clearLog,negativeSpacer,emptyBtn1, reloadApp,negativeSpacer,emptyBtn1, stopApp, negativeSpacer,emptyBtn1,showTimes, negativeSpacer,emptyBtn1,clearData,negativeSpacer,emptyBtn1,copyLog,negativeSpacer, emptyBtn1,closeLogView, nil]];
    [toolbar setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];

    UIView *tableViewWrapper = [[UIView alloc] initWithFrame:CGRectMake(0, 80,  currentVc.frame.size.width, currentVc.frame.size.height - 80)];

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,  tableViewWrapper.frame.size.width, tableViewWrapper.frame.size.height)];

    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"LogCell"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.tableView.contentInset = UIEdgeInsetsMake(3.0, 0.0, 0.0, 0.0);
     });

    [tableViewWrapper addSubview:self.tableView];

    [logView addSubview:toolbar];
    [logView addSubview:tableViewWrapper];

    [currentVc addSubview:logView];

}


//Close button pressed
- (void) handleExit {

    [logView removeFromSuperview];

}

//clearLogPressed
- (void) clearLogPressed {

    NSMutableArray *mutableMessageArray = [self.logArray mutableCopy];
    [mutableMessageArray removeAllObjects];
    self.logArray = mutableMessageArray;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@""
                         forKey:@"logArray"];
       [userDefaults synchronize];
    _forceCount = 0;
    _errorsCount = 0;
    _warnCount = 0;
    _infoCount = 0;
    _debugCount = 0;

    [[AppDownloadViewHelper sharedInstance].tableView reloadData];

    [[CFRuntime instance] changeWarnLabel:@"0"];
    [[CFRuntime instance] changeErrorLabel:@"0"];

}

//reloadAppPressed
- (void) reloadAppPressed {
      [logView removeFromSuperview];
      NSURL *url = [[NSURL alloc] initWithString:_lastUrlString];
    
        if (!url) {
            url = [NSURL fileURLWithPath:[CFRuntime pathForAppFile:@"Data" ofType:@"xml" inDirectory:nil]];
        }
    
        if (![[CFRuntime instance] handleAppUpgrade])
        {
            NSLog(@"Failed to upgrade app");
        }
    
        if (![[CFRuntime instance] loadAppFromUrl:url]) {
            [BundleSettings setOverrideRemoteApp:NO];
            [[[UIAlertView alloc] initWithTitle:@"Failed" message:@"Failed to load App" cancelButtonItem:[RIButtonItem itemWithLabel:@"OK" andAction:^() {
                if ([BundleSettings isRemoteApp]) {
                    //[self showAppDownloadViewController];

                }
            }] otherButtonItems: nil] show];
        } else {
            CFPage* rootPage = [[CFRuntime instance] getRootPage];

            if ([[AppDownloadViewHelper sharedInstance].overlayDebugSwitch isEqual:@"true"]) {

                [CFRuntime presentPage:rootPage withDebugoverlayInPosition:@"Left"];
    
            } else {
                [CFRuntime presentPage:rootPage];
            }
        }
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:[AppDownloadViewHelper sharedInstance].logArray
                     forKey:@"logArray"];
        [userDefaults synchronize];

        NSMutableArray *mutableMessageArray = [self.logArray mutableCopy];
        [mutableMessageArray removeAllObjects];
        self.logArray = mutableMessageArray;

        _forceCount  = 0;
        _errorsCount = 0;
        _warnCount   = 0;
        _infoCount   = 0;
        _debugCount  = 0;
        [[CFRuntime instance] changeWarnLabel:@"0"];
        [[CFRuntime instance] changeErrorLabel:@"0"];
}

//stopAppPressed
- (void) stopAppPressed {

    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].logArray
                     forKey:@"logArray"];
    [userDefaults synchronize];

    NSMutableArray *mutableMessageArray = [self.logArray mutableCopy];
    [mutableMessageArray removeAllObjects];
    self.logArray = mutableMessageArray;

    [logView removeFromSuperview];

    _forceCount  = 0;
    _errorsCount = 0;
    _warnCount   = 0;
    _infoCount   = 0;
    _debugCount  = 0;

    if (self.delegate) {
        [self.delegate reloadWithUrl:nil];
    }
}

//showTimePressed
- (void) showTimePressed {

        if (ButtonSelected == 0)
        {
            _showTimes = @"true";
            [self.showTimesbutton setTitle:NSLocalizedString(@"Hide\ntimes", nil) forState:UIControlStateNormal];
            ButtonSelected = 1;
        }

        else if (ButtonSelected == 1)
        {
            _showTimes = @"false";
                        [self.showTimesbutton setTitle:NSLocalizedString(@"Show\ntimes", nil) forState:UIControlStateNormal];
            ButtonSelected = 0;
        }
    if (_logArray || self.logArray.count){
        self.labelOneWidth = [self sizeOfMultiLineLabelWithString:self.labelTime.text].width + 7;
        self.labelMsgWidth = self.tableView.frame.size.width - self.labelOneWidth;
        
        [self.tableView reloadData];
    }
}

//clearDataPressed
- (void) clearDataPressed {
    if (self.delegate) {
        [self.delegate removeLocalStorage];
    }
   UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Clear Data" message:@"Success" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];

    [alert show];
}

//Copy button pressed
- (void) copyLogPressed {

    NSString *time;
    NSString *str;
    UIAlertView *alert;

    if (_logArray || self.logArray.count){

    for (NSDictionary *logs in self.logArray) {

        if ([_showTimes isEqual:@"true"]) {
             time = [NSString stringWithFormat:@"%@", logs[@"time"]] ;

        }
        else {
             time = @"";

        }
        NSString *msg  = [NSString stringWithFormat:@"%@", logs[@"message"]];


        NSString *cpResult = [NSString stringWithFormat:@"%@ %@ \n ",time ,msg] ;

        [_cpArr addObject:cpResult];

    }

    NSMutableString * result = [[NSMutableString alloc] init];
    for (NSObject * obj in _cpArr)
    {
        [result appendString:[obj description]];
    }
    str = [NSString stringWithFormat:@"%@", result];
    alert = [[UIAlertView alloc] initWithTitle:@"Copy Logs" message:@"Logs was copied to your clipboard" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
    else{
        str = @"";
        alert = [[UIAlertView alloc] initWithTitle:@"Copy Logs" message:@"Empty Logs" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    }
    [UIPasteboard generalPasteboard].string = str;
    [alert show];

}
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

   return self.logArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView setSeparatorColor:[UIColor whiteColor]]; //or your background color

    static NSString *CellIdentifier = @"LogCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath] ;

    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    self.labelMsg = [UILabel new];

    self.labelMsg.text = [[self.logArray valueForKey:@"message"] objectAtIndex:indexPath.row];

    self.currentFont = self.labelMsg.font;
    self.labelMsg.numberOfLines = 0;
    self.labelMsg.lineBreakMode = NO;
    [self.labelMsg sizeToFit];

    self.labelTime = [UILabel new];

    CGFloat paddingLeft;
    if ([_showTimes isEqual:@"true"]) {
        self.labelTime.text = [[self.logArray valueForKey:@"time"] objectAtIndex:indexPath.row];
        paddingLeft = 7;
    }else{
        self.labelTime.text = @"";
        paddingLeft = 0;
    }
    
    self.currentFont = self.labelTime.font;
    self.labelTime.numberOfLines = 0;
    self.labelTime.lineBreakMode = NO;
    [self.labelTime sizeToFit];

    self.cellHeight = [self sizeOfMultiLineLabelWithString:self.labelMsg.text].height;
    self.labelOneWidth = [self sizeOfMultiLineLabelWithString:self.labelTime.text].width + 7;
    self.labelMsgWidth = self.tableView.frame.size.width - self.labelOneWidth;

    UILabel *labelOne = [[UILabel alloc] initWithFrame:CGRectMake(paddingLeft, 2, self.labelOneWidth , self.cellHeight +2)];
    UILabel *labelTwo = [[UILabel alloc]initWithFrame:CGRectMake(self.labelOneWidth + paddingLeft , 2, self.labelMsgWidth, self.cellHeight +2)];

    labelOne.text = self.labelTime.text;

    [labelOne setFont:[UIFont systemFontOfSize:12]];
    labelOne.numberOfLines = 0;
    labelOne.lineBreakMode = NO;
    labelOne.textAlignment = NSTextAlignmentLeft;
    labelOne.tag = 1;
    [labelOne setBackgroundColor:[UIColor clearColor]];
    [labelOne sizeToFit];

    labelTwo.text = self.labelMsg.text;
    [labelTwo setTextColor:[UIColor colorFromHexString:[[self.logArray valueForKey:@"color"] objectAtIndex:indexPath.row]]];

    [labelTwo setFont:[UIFont systemFontOfSize:12]];
    labelTwo.textAlignment = NSTextAlignmentLeft;
    labelTwo.lineBreakMode = NO;
    labelTwo.numberOfLines = 0;
    [labelTwo sizeToFit];
    labelTwo.tag = 1;
    [labelTwo setBackgroundColor:[UIColor clearColor]];


    if ((([cell.contentView viewWithTag:1]) && ([cell.contentView viewWithTag:2])))
    {
        [[cell.contentView viewWithTag:1]removeFromSuperview];
        [[cell.contentView viewWithTag:2]removeFromSuperview];
    }

    [cell.contentView addSubview:labelOne];
    [cell.contentView addSubview:labelTwo];


    return cell;

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.cellHeight + 2 ;
}



- (CGSize) sizeOfMultiLineLabelWithString: (NSString *)labelText {

    //Label text
    NSString *aLabelTextString = labelText;

    //Label font
    UIFont *aLabelFont = [UIFont systemFontOfSize:12];

    //Width of the Label
    CGFloat aLabelSizeWidth =  self.labelMsgWidth;


    if (SYSTEM_VERSION_LESS_THAN(iOS7_0)) {
        //version < 7.0

        return [aLabelTextString sizeWithFont:aLabelFont
                            constrainedToSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                lineBreakMode:NSLineBreakByWordWrapping];
    }
    else if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(iOS7_0)) {
        //version >= 7.0

        //Return the calculated size of the Label
        return [aLabelTextString boundingRectWithSize:CGSizeMake(aLabelSizeWidth, MAXFLOAT)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{
                                                        NSFontAttributeName : aLabelFont
                                                        }
                                              context:nil].size;

    }
    
    return logView.frame.size;
}


#pragma mark UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}


@end
