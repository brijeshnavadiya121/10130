//
//  AppDownloadViewController.m
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "AppDownloadViewController.h"
#import "RecentList.h"
#import "ScannerViewController.h"
#import "FacebookHelper.h"


@interface AppDownloadViewController () <AppDownloadViewControllerDelegate, ScannerViewControllerDelegate, UITextFieldDelegate>


@property (strong, nonatomic) IBOutlet UITextField *txtUri;
@property (strong, nonatomic) IBOutlet UINavigationItem *navItem;

@property (weak, nonatomic) IBOutlet UIButton *btnLoad;
@property (weak, nonatomic) IBOutlet UIButton *btnScan;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UIButton *btnTopLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnTopRight;
@property (weak, nonatomic) IBOutlet UIButton *btnBotLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnBotRight;
@property (weak, nonatomic) IBOutlet UIButton *btnViewLog;
@property (strong, nonatomic) NSString *overlayDebugViewIsOn;
@property (strong, nonatomic) NSString *errorsIsOn;
@property (strong, nonatomic) NSString *warningsIsOn;
@property (strong, nonatomic) NSString *informationsIsOn;
@property (strong, nonatomic) NSString *debugIsOn;

@end


@implementation AppDownloadViewController

+ (AppDownloadViewController *)sharedInstance {
    static dispatch_once_t onceToken;
    static AppDownloadViewController *instance = nil;
    dispatch_once(&onceToken, ^{
        instance = [[AppDownloadViewController alloc] init];
    });
    return instance;
}

- (id)init {
    self = [super init];

    if (self) {

    }

    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

   UITapGestureRecognizer * tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];



    self.navigationItem.title = self.navItem.title;
    self.navigationItem.rightBarButtonItems = self.navItem.rightBarButtonItems;
    self.navigationItem.backBarButtonItem = self.navItem.backBarButtonItem;
    
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    
    self.lblVersion.text = appBuildString;
    [_txtUri setDelegate:self];
    

    NSString *overlayDebugSwitch = [[NSUserDefaults standardUserDefaults] objectForKey:@"overlayDebugSwitch"];
    NSString *btnPosition = [[NSUserDefaults standardUserDefaults] objectForKey:@"btnPosition"];
    NSString *errorsSwitch = [[NSUserDefaults standardUserDefaults] objectForKey:@"errorsSwitch"];
    NSString *warningsSwitch = [[NSUserDefaults standardUserDefaults] objectForKey:@"warningsSwitch"];
    NSString *infoSwitch = [[NSUserDefaults standardUserDefaults] objectForKey:@"infoSwitch"];
    NSString *debugSwitch = [[NSUserDefaults standardUserDefaults] objectForKey:@"debugSwitch"];


    if (overlayDebugSwitch != nil) {
        [AppDownloadViewHelper sharedInstance].overlayDebugSwitch   = overlayDebugSwitch;

    }else{
        [AppDownloadViewHelper sharedInstance].overlayDebugSwitch   = @"false";

    }

    if (btnPosition != nil) {
        [AppDownloadViewHelper sharedInstance].btnPosition   = btnPosition;

    }else{
        [AppDownloadViewHelper sharedInstance].btnPosition   = @"botRight";
    }

    if (errorsSwitch != nil){
        [AppDownloadViewHelper sharedInstance].errorsSwitch   = errorsSwitch;

    }else{
        [AppDownloadViewHelper sharedInstance].errorsSwitch   = @"false";
    }

    if (warningsSwitch != nil) {
        [AppDownloadViewHelper sharedInstance].warningsSwitch   = warningsSwitch;

    }else{
        [AppDownloadViewHelper sharedInstance].warningsSwitch   = @"false";
    }

    if (infoSwitch != nil) {
        [AppDownloadViewHelper sharedInstance].infoSwitch   = infoSwitch;

    }else{
        [AppDownloadViewHelper sharedInstance].infoSwitch   = @"false";
    }

    if (debugSwitch != nil) {
        [AppDownloadViewHelper sharedInstance].debugSwitch   = debugSwitch;

    }else{
        [AppDownloadViewHelper sharedInstance].debugSwitch   = @"false";
    }


    if ([[AppDownloadViewHelper sharedInstance].overlayDebugSwitch isEqual:@"true"]) {
        [self.overlay setOn:YES];

    }
    else{
        [self.overlay setOn:NO];

    }

    if ([[AppDownloadViewHelper sharedInstance].errorsSwitch isEqual:@"true"]) {
        [self.errors setOn:YES];

    }
    else {
        [self.errors setOn:NO];
    }


    if ([[AppDownloadViewHelper sharedInstance].warningsSwitch isEqual:@"true"]) {
        [self.warnings setOn:YES animated:YES];

    }
    else{
        [self.warnings setOn:NO];

    }
    if ([[AppDownloadViewHelper sharedInstance].infoSwitch isEqual:@"true"]) {
        [self.info setOn:YES];

    }
    else{
        [self.info setOn:NO];

    }
    if ([[AppDownloadViewHelper sharedInstance].debugSwitch isEqual:@"true"]) {
        [self.debug setOn:YES];

    }
    else{
        [self.debug setOn:NO];

    }
    //Load button
    self.btnLoad.backgroundColor = [UIColor colorFromHexString:@"46ca5e"];
    self.btnLoad.tintColor =[UIColor whiteColor];
    self.btnLoad.layer.cornerRadius = 5;

    //Scan Button
    self.btnScan.backgroundColor = [UIColor colorFromHexString:@"47b0ed"];
    self.btnScan.tintColor =[UIColor whiteColor];
    self.btnScan.layer.cornerRadius = 5;

    //Top Right Button
    self.btnTopRight.backgroundColor = [UIColor colorFromHexString:@"46ca5e"];
    self.btnTopRight.tintColor =[UIColor whiteColor];
    self.btnTopRight.layer.cornerRadius = 5;

    //Top Left Button
    self.btnTopLeft.backgroundColor = [UIColor colorFromHexString:@"46ca5e"];
    self.btnTopLeft.tintColor =[UIColor whiteColor];
    self.btnTopLeft.layer.cornerRadius = 5;

    //Bot Right Button
    self.btnBotRight.backgroundColor = [UIColor colorFromHexString:@"46ca5e"];
    self.btnBotRight.tintColor =[UIColor whiteColor];
    self.btnBotRight.layer.cornerRadius = 5;

    //Bot Left Button
    self.btnBotLeft.backgroundColor = [UIColor colorFromHexString:@"46ca5e"];
    self.btnBotLeft.tintColor =[UIColor whiteColor];
    self.btnBotLeft.layer.cornerRadius = 5;

    //View Log Button
    self.btnViewLog.backgroundColor = [UIColor colorFromHexString:@"47b0ed"];
    self.btnViewLog.tintColor =[UIColor whiteColor];
    self.btnViewLog.layer.cornerRadius = 5;

    NSString* lastUsedEntry = [[[RecentList getInstance] getRecentList] firstObject];
    self.txtUri.text = lastUsedEntry;
    [self txtValueChanged:self.txtUri];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loadPressed:(id)sender {
    
    [[RecentList getInstance] addEntry:self.txtUri.text];
    [AppDownloadViewHelper sharedInstance].lastUrlString = self.txtUri.text;

    [AppDownloadViewHelper sharedInstance].logArray = [[NSMutableArray alloc] init];

    if (self.delegate) {

        [self.delegate viewController:self urlSelected:self.txtUri.text];
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];

    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].btnPosition
                     forKey:@"btnPosition"];
    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].overlayDebugSwitch
                     forKey:@"overlayDebugSwitch"];
    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].errorsSwitch
                     forKey:@"errorsSwitch"];
    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].warningsSwitch
                     forKey:@"warningsSwitch"];
    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].infoSwitch
                     forKey:@"infoSwitch"];
    [userDefaults setObject:[AppDownloadViewHelper sharedInstance].debugSwitch
                     forKey:@"debugSwitch"];

    [userDefaults synchronize];

}

- (IBAction)historyPressed:(id)sender {
    
    RecentListViewController* viewController = [[RecentListViewController alloc] init];
    viewController.delegate = self;
    
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)scanPressed:(id)sender {
    ScannerViewController* viewController = [[ScannerViewController alloc] init];
    viewController.delegate = self;
    
    BaseNavigationController* container = [[BaseNavigationController alloc] initWithRootViewController:viewController];
    
    [self presentViewController:container animated:YES completion:nil];
}

- (IBAction)topLeftPressed:(UIButton *)sender {

    [AppDownloadViewHelper sharedInstance].btnPosition = @"topLeft";

    self.btnTopLeft.enabled     = NO;
    self.btnTopRight.enabled    = YES;
    self.btnBotLeft.enabled     = YES;
    self.btnBotRight.enabled    = YES;

}

- (IBAction)topRightPressed:(UIButton *)sender {

    [AppDownloadViewHelper sharedInstance].btnPosition = @"topRight";

    self.btnTopLeft.enabled     = YES;
    self.btnTopRight.enabled    = NO;
    self.btnBotLeft.enabled     = YES;
    self.btnBotRight.enabled    = YES;
}

- (IBAction)botLeftPressed:(UIButton *)sender {

    [AppDownloadViewHelper sharedInstance].btnPosition = @"botLeft";

    self.btnTopLeft.enabled     = YES;
    self.btnTopRight.enabled    = YES;
    self.btnBotLeft.enabled     = NO;
    self.btnBotRight.enabled    = YES;
}

- (IBAction)botRightPressed:(UIButton *)sender {

    [AppDownloadViewHelper sharedInstance].btnPosition = @"botRight";

    self.btnTopLeft.enabled     = YES;
    self.btnTopRight.enabled    = YES;
    self.btnBotLeft.enabled     = YES;
    self.btnBotRight.enabled    = NO;
}

- (IBAction)viewLogPressed:(id)sender {

    NSMutableArray *userLogArray = [[NSUserDefaults standardUserDefaults] objectForKey:@"logArray"];

    if ((userLogArray != nil )) {

        if ([userLogArray isEqual:@""]) {
            userLogArray = [[NSMutableArray alloc] init];
        }

        [AppDownloadViewHelper sharedInstance].logArray = userLogArray;
    }

    [[AppDownloadViewHelper sharedInstance] openLogView];

}

- (IBAction)debugOverlyEnablePressed:(id)sender {

    if([sender isOn]){
        [AppDownloadViewHelper sharedInstance].overlayDebugSwitch = @"true";

    } else{
        [AppDownloadViewHelper sharedInstance].overlayDebugSwitch = @"false";
    }
}

- (IBAction)errorSwitch:(id)sender {
    if([sender isOn]){
        [AppDownloadViewHelper sharedInstance].errorsSwitch = @"true";

    } else{
        [AppDownloadViewHelper sharedInstance].errorsSwitch = @"false";
    }
}

- (IBAction)warningsSwitch:(id)sender {
    if([sender isOn]){
        [AppDownloadViewHelper sharedInstance].warningsSwitch = @"true";

    } else{
        [AppDownloadViewHelper sharedInstance].warningsSwitch = @"false";
    }
}

- (IBAction)infoSwitch:(id)sender {
    if([sender isOn]){
        [AppDownloadViewHelper sharedInstance].infoSwitch = @"true";

    } else{
        [AppDownloadViewHelper sharedInstance].infoSwitch = @"false";
    }
}

- (IBAction)debugSwitch:(id)sender {
    if([sender isOn]){
        [AppDownloadViewHelper sharedInstance].debugSwitch = @"true";

    } else{
        [AppDownloadViewHelper sharedInstance].debugSwitch = @"false";
    }
}

- (IBAction)txtValueChanged:(id)sender {
    self.btnLoad.enabled = ![NSString isEmpty:self.txtUri.text];
}

#pragma mark AppDownloadViewControllerDelegate

-(void) viewController:(id)viewController urlSelected:(NSString *)url {
    self.txtUri.text = url;
    [self txtValueChanged:self.txtUri];

    [self.navigationController popToViewController:self animated:YES];

}

#pragma mark ScannerViewControllerDelegate

- (void) scanner:(ScannerViewController*) viewController didCaptureCode:(NSString*) code {
    if (code) {
        self.txtUri.text = code;
        [self txtValueChanged:self.txtUri];
    }
 
    [self.presentedViewController dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}
@end
