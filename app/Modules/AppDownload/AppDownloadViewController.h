//
//  AppDownloadViewController.h
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseViewController.h"
#import "RecentListViewController.h"
#import "AppDownloadViewHelper.h"

@interface AppDownloadViewController : BaseViewController

@property (weak, nonatomic) id<AppDownloadViewControllerDelegate> delegate;

- (IBAction)loadPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *overlay;
@property (weak, nonatomic) IBOutlet UISwitch *errors;
@property (weak, nonatomic) IBOutlet UISwitch *info;
@property (weak, nonatomic) IBOutlet UISwitch *debug;
@property (weak, nonatomic) IBOutlet UISwitch *warnings;


@end
