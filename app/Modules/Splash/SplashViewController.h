//
//  SplashViewController.h
//  app
//
//  Created by Bernhard Kunnert on 04.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SplashViewController : BaseViewController

@end
