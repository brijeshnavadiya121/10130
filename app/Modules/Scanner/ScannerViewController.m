//
//  ScannerViewController.m
//  app
//
//  Created by Bernhard Kunnert on 07.01.14.
//  Copyright (c) 2014 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "ScannerViewController.h"
#import <ZXingObjC.h>

@interface ScannerViewController () <ZXCaptureDelegate>

@property (nonatomic, strong) ZXCapture* capture;

@end

@implementation ScannerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    self.capture = [[ZXCapture alloc] init];
    self.capture.rotation = 90.0f;
    
    ZXDecodeHints* hints = [ZXDecodeHints hints];
    [hints addPossibleFormat:kBarcodeFormatQRCode];
    
    self.capture.hints = hints;
    
    // Use the back camera
    self.capture.camera = self.capture.back;
    
    self.capture.layer.frame = self.view.bounds;
    [self.view.layer addSublayer:self.capture.layer];

    UIBarButtonItem* closeButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPressed:)];
    
    self.navigationItem.rightBarButtonItem = closeButton;
    self.title = @"Scan QR Code";
     
}

- (void) cancelPressed:(id) sender
{
    if (self.delegate) {
        [self.delegate scanner:self didCaptureCode:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.capture.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.capture.hints = nil;
    [self.capture.layer removeFromSuperlayer];
    self.capture.delegate = nil;
    [self.capture stop];
    
}

#pragma mark - ZXCaptureDelegate Methods

- (void)captureResult:(ZXCapture*)capture result:(ZXResult*)result {
    NSString* code;
    if (result) {
        code = result.text;
    }
    
    if (self.delegate) {
        [self.delegate scanner:self didCaptureCode:code];
    }
}

- (void)captureSize:(ZXCapture*)capture width:(NSNumber*)width height:(NSNumber*)height {
}

@end
