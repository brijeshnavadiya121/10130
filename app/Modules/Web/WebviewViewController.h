//
//  WebviewViewController.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationViewController.h"
#import "LocationTracker.h"
#import "MenuPositionHelper.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <WebKit/WebKit.h>

@interface WebviewViewController : NavigationViewController <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

@property (nonatomic, strong) NSMutableDictionary *payPalResult;

//Location
@property LocationTracker * locationTracker;
@property (nonatomic) NSTimer* locationUpdateTimer;
@property (strong,nonatomic) LocationShareModel * shareModel;

//@property (nonatomic, strong) void (^ completionHandler)(NSString *);

@end
