//
//  GoogleAnalyticsViewController.m
//  app
//
//  Created by LeyLa on 4/12/16.
//  Copyright © 2016 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "GoogleAnalyticsViewController.h"
#import <Google/Analytics.h>
//#import <FirebaseCore/FirebaseCore.h>
//@import FirebaseAnalytics;

@interface GoogleAnalyticsViewController ()

@end

@implementation GoogleAnalyticsViewController


- (void) registerPage:(NSString *)pageName {
    
    NSString *name;
    if (![pageName isEqual:@""]) {
        
        name = [NSString stringWithFormat:@"%@", pageName];

        // The UA-XXXXX-Y tracker ID is loaded automatically from the
        // GoogleService-Info.plist by the `GGLContext` in the AppDelegate.
        // If you're copying this to an app just using Analytics, you'll
        // need to configure your tracking ID here.
        // [START screen_view_hit_objc]
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];

        [tracker set:kGAIScreenName value:name];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        // [END screen_view_hit_objc]
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Empty data!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

// Send Event
- (void) sendEventWithCategory:(NSString *)category andAction:(NSString *) action andLabel:(NSString *) label andValue:(NSString *) value {
    
    if ((![category isEqual:@""]) || (![action isEqual:@""]) || (![label isEqual:@""]) || (![value isEqual:@""])) {
        
        _category = category;
        _action   = action;
        _label    = label;
        _value    = value;
        
        NSNumber *valueNum = [NSNumber numberWithInteger: [_value integerValue]];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:_category
                                                              action:_action
                                                               label:_label
                                                               value:valueNum] build]];
        
        
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Empty data!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}


// Send Product (Google Ecommerce)

- (void) sendProductWithTransaction:(NSString *)transactionId andName:(NSString *) name andSku:(NSString *) sku andCategory:(NSString *) category andPrice:(NSString *) price andQuantity:(NSString *) quantity andCurrencyCode:(NSString *) currencyCode {
    
    
    if ((![transactionId isEqual:@""]) || (![name isEqual:@""]) || (![sku isEqual:@""]) || (![category isEqual:@""]) || (![price isEqual:@""]) || (![quantity isEqual:@""]) || (![currencyCode isEqual:@""])) {
        
        _transactionId = transactionId;
        _name          = name;
        _sku           = sku;
        _category      = category;
        _price         = price;
        _quantity      = quantity;
        _currencyCode  = currencyCode;
        
        NSNumber *priceNum   = [NSNumber numberWithInteger: [_price integerValue]];
        NSNumber *quanityNum = [NSNumber numberWithInteger: [_quantity integerValue]];
        
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        
            [tracker send:[[GAIDictionaryBuilder createItemWithTransactionId:_transactionId
                                                                        name:_name
                                                                         sku:_sku
                                                                    category:_category
                                                                       price:priceNum
                                                                    quantity:quanityNum
                                                                currencyCode:_currencyCode] build]];
            
            

    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Empty data!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

// Send Transaction (Google Ecommerce)
- (void) sendTransactionWithTransaction:(NSString *)transactionId andAffiliation:(NSString *) affiliation andRevenue:(NSString *) revenue andTax:(NSString *) tax andShipping:(NSString *) shipping andCurrencyCode:(NSString *) currencyCode {
    
    
    if ((![transactionId isEqual:@""]) || (![affiliation isEqual:@""]) || (![revenue isEqual:@""]) || (![tax isEqual:@""]) || (![shipping isEqual:@""]) || (![currencyCode isEqual:@""])) {
        
        _transactionTransactionId = transactionId;
        _affiliation              = affiliation;
        _revenue                  = revenue;
        _tax                      = tax;
        _shipping                 = shipping;
        _transactionCurrencyCode  = currencyCode;
        
        NSNumber *revenueNum  = [NSNumber numberWithInteger: [_revenue integerValue]];
        NSNumber *taxNum      = [NSNumber numberWithInteger: [_tax integerValue]];
        NSNumber *shippingNum = [NSNumber numberWithInteger: [_shipping integerValue]];
        
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        
        
            [tracker send:[[GAIDictionaryBuilder createTransactionWithId:_transactionTransactionId
                                                             affiliation:_affiliation
                                                                 revenue:revenueNum
                                                                     tax:taxNum
                                                                shipping:shippingNum
                                                            currencyCode:_transactionCurrencyCode] build]];
           } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"Empty data!"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
}

#pragma mark Firebase
//
////Set user property
//- (void) setUserProperty:(NSString *) property forName:(NSString *)name {
//    
//    [FIRAnalytics setUserPropertyString:property forName:name];
//    
//    NSString *title = [NSString stringWithFormat:@"Set User Property"];
//    NSString *message = [NSString stringWithFormat:@"Set user property: %@ forName: %@", property, name];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                  message:message
//                                               delegate:nil
//                                    cancelButtonTitle:@"Ok"
//                                  otherButtonTitles:nil];
//    [alert show];
//    
//}
//
////Log Event
//- (void) logEventWithName:(NSString *) name {
//    [FIRAnalytics logEventWithName:@"CustomEventLog"
//                        parameters:nil];
//    
//    NSString *title = [NSString stringWithFormat:@"Log custom event"];
//    NSString *message = [NSString stringWithFormat:@"Log evet with name %@", name];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                    message:message
//                                                   delegate:nil
//                                          cancelButtonTitle:@"Ok"
//                                          otherButtonTitles:nil];
//    [alert show];
//}
//
////Set User Id
//- (void) setUserId:(NSString *)userId {
//    [FIRAnalytics setUserID:userId];
//    
//    NSString *title = [NSString stringWithFormat:@"Set User Id"];
//    NSString *message = [NSString stringWithFormat:@"User Id %@", userId];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                    message:message
//                                                   delegate:nil
//                                          cancelButtonTitle:@"Ok"
//                                          otherButtonTitles:nil];
//    [alert show];
//}
//
////Handle Open Url
//- (void) handleOpenURL:(NSString *) stringURL {
//    
//
//    NSURL *url = [[NSURL alloc] initWithString:stringURL];
//
//    [FIRAnalytics handleOpenURL:url];
//    
//    NSString *title = [NSString stringWithFormat:@"Handle Open Url"];
//    NSString *message = [NSString stringWithFormat:@"URL: %@", stringURL];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                    message:message
//                                                   delegate:nil
//                                          cancelButtonTitle:@"Ok"
//                                          otherButtonTitles:nil];
//    [alert show];
//}
//
////HandleEventsForBackgroundURLSession
//- (void) handleEventsForBackgroundURLSession:(NSString *) session  {
//
//    [FIRAnalytics handleEventsForBackgroundURLSession:session completionHandler:^{
//        
//        //Session
//        
//        NSString *title = [NSString stringWithFormat:@"handleEventsForBackgroundURLSession"];
//        NSString *message = [NSString stringWithFormat:@"Session: %@", session];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                        message:message
//                                                       delegate:nil
//                                              cancelButtonTitle:@"Ok"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }];
//
//}
//
////HandleUserActivity
//- (void) handleUserActivity:(NSString *) activity {
//    
//  //  [FIRAnalytics handleUserActivity:activity];
//
//[FIRAnalytics handleUserActivity:nil];
//    
//    NSString *title = [NSString stringWithFormat:@"handleUserActivity"];
//    NSString *message = [NSString stringWithFormat:@"Activitys: %@", activity];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//                                                    message:message
//                                                   delegate:nil
//                                          cancelButtonTitle:@"Ok"
//                                          otherButtonTitles:nil];
//    [alert show];
//}
//
//    // [START custom_event_objc]
////    [FIRAnalytics logEventWithName:kFIREventSelectContent parameters:@{
////                                                                       kFIRParameterContentType:@"cont",
////                                                                       kFIRParameterItemID:@"1"
////                                                                       }];
//    // [END custom_event_objc]
//    
//    // [START custom_event_objc]
//   // [FIRAnalytics logEventWithName:@"share_image"
//     //                   parameters:@{
//       //                              @"name": @"df",
//         //                            @"full_text": @"dsf"
//           //                          }];
//    // [END custom_event_objc]
//    //NSString *title = [NSString stringWithFormat:@"Share: TITLEL"];
//    //NSString *message =
//    //@"Share event sent to Analytics; actual share not implemented in this quickstart";
//    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
//      //                                              message:message
//        //                                           delegate:nil
//          //                                cancelButtonTitle:@"Ok"
//            //                              otherButtonTitles:nil];
//    //[alert show];
//    

@end
