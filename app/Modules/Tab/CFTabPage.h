//
//  CFTabPage.h
//  app
//
//  Created by Bernhard Kunnert on 05.11.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CFPage.h"
#import "BSJsonSupported.h"
#import "TypedPage.h"

@interface CFTabPage : NSObject <BSJsonSupported, TypedPage>

@property (nonatomic) NSArray* tabItems;
@property (nonatomic) NSInteger selectedTabBarIndex;
@property (strong, nonatomic, readonly) CFPage* page;

- (id) initWithPage:(CFPage*) page;

@end
