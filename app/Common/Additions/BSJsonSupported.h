//
//  BSJsonSupported.h
//  app
//
//  Created by Bernhard Kunnert on 01.10.15.
//  Copyright (c) 2015 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BSJsonSupported <NSObject>

@required

- (id)proxyForJson;
- (instancetype) initWithJson:(NSDictionary*) receivedObjects;

@end
