//
//  UIImage+fixOrientation.h
//  app
//
//  Created by LeyLa on 11/3/17.
//  Copyright © 2017 CloudFaces. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
