//
//  RIButtonItem+Action.m
//  Pods
//
//  Created by Bernhard Kunnert on 04.11.13.
//
//

#import "RIButtonItem+Action.h"


@implementation RIButtonItem (Action)

+ (id) itemWithLabel:(NSString *)inLabel andAction:(void (^)())action {
    RIButtonItem* item = [self itemWithLabel:inLabel];
    item.action = action;
    
    return item;
}

@end
