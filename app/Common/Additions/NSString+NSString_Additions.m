//
//  NSString+NSString_Additions.m
//  mobile-pocket
//
//  Created by Simon Moser on 04.11.11.
//  Copyright (c) 2011 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "NSString+NSString_Additions.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (NSString_Additions)

- (NSString*)MD5
{
    // Create pointer to the string as UTF8
    const char *ptr = [self UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, (int) strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) 
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}


- (NSString*) numberStringInColumns:(NSInteger) columnWidth
{
    int count = 0;
    NSMutableString* result = [[NSMutableString alloc] initWithCapacity:10];
    for (int i = 0; i < self.length; i++) {
        BOOL bAddSpace = NO;
        unichar c = [self characterAtIndex:i];
        if (c >= '0' && c <= '9')
        {
            count++;
        }
        else
        {
            count = 0;
        }
        
        if (count == 3)
        {
            count = 0;
            bAddSpace = YES;
        }
        
        
        [result appendString:[NSString stringWithFormat:@"%c", [self characterAtIndex:i]]];
        if (bAddSpace)
        {
            [result appendString:@" "];
        }
    }
    return [result description];
}

-(NSString*) stripHtml 
{
    NSString* startString = [self stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    startString = [startString stringByReplacingOccurrencesOfString:@"<br />" withString:@"\n"];
    startString = [startString stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    startString = [startString stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    startString = [startString stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    startString = [startString stringByReplacingOccurrencesOfString:@"&gt;;" withString:@">"];
    
    NSMutableString *html = [NSMutableString stringWithCapacity:[startString length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:startString];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    return html;
}

- (BOOL) isEmpty 
{
    return [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0;
}

+ (BOOL) isEmpty:(NSString *)string {
    return !string || [string isEmpty];
}

@end
