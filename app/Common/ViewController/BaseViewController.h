//
//  BaseViewController.h
//  app
//
//  Created by Simon Moser on 24.02.12.
//  Copyright (c) 2012 bluesource - mobile solutions gmbh. All rights reserved.
//

@interface BaseViewController : UIViewController
{
    BundleSettings* bs;
}

@property (nonatomic, retain) BundleSettings *bs;

- (void)startWaitingAnimation;
- (void)stopWaitingAnimaiton;

@end
