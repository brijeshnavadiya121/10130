//
//  BaseNavigationController.h
//  app
//
//  Created by Bernhard Kunnert on 18.12.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseNavigationController : UINavigationController
@property(nonatomic) NSUInteger rotationMask;
@property(nonatomic) BOOL shouldRotate;
@end
