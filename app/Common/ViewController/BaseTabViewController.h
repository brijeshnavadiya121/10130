//
//  BaseTabViewController.h
//  app
//
//  Created by Bernhard Kunnert on 18.12.13.
//  Copyright (c) 2013 bluesource - mobile solutions gmbh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTabViewController : UITabBarController <UITabBarDelegate, UITabBarControllerDelegate>

- (void) makeTabBarHidden:(BOOL)hide animated:(BOOL) animated;

@property BOOL tabBarHidden;

@end
