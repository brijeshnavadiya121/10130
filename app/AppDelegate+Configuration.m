//
//  AppDelegate+Configuration.m
//  mobile-pocket
//
//  Created by Simon Moser on 20.12.11.
//  Copyright (c) 2011 bluesource - mobile solutions gmbh. All rights reserved.
//

#import "AppDelegate+Configuration.h"
#import "StringDefines.h"

@interface NSManagedObjectContext ()
+ (void)MR_setRootSavingContext:(NSManagedObjectContext *)context;
+ (void)MR_setDefaultContext:(NSManagedObjectContext *)moc;
@end

@implementation AppDelegate (Configuration)

-(void) initRestKit
{


    // Init core data data model
    /*
    NSURL* modelUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Model" ofType:@"momd"]];
    NSManagedObjectModel* managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelUrl] mutableCopy];
    RKManagedObjectStore* managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:DATABSE_NAME];
    NSError *error = nil;
    [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:&error];
    [managedObjectStore createManagedObjectContexts];
    
    // Configure MagicalRecord to use RestKit's Core Data stack
    [NSPersistentStoreCoordinator MR_setDefaultStoreCoordinator:managedObjectStore.persistentStoreCoordinator];
    [NSManagedObjectContext MR_setRootSavingContext:managedObjectStore.persistentStoreManagedObjectContext];
    [NSManagedObjectContext MR_setDefaultContext:managedObjectStore.mainQueueManagedObjectContext];
    
    
    BundleSettings* bs = [BundleSettings instance];
    NSString* serverUrl = [bs serverUrl];
    if(serverUrl == nil) {
        serverUrl = BSStaticTargetConfig(@"DefaultServerUrl");
        [bs setServerUrl:serverUrl];
    }
    
    // Init Restkit object manager
    RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:serverUrl]];
    
    [RKObjectManager setSharedManager:objectManager];
    
    [RKObjectManager sharedManager].requestSerializationMIMEType = RKMIMETypeJSON;
    
#if DEBUG
    // turn on detailed logging
    // Log all HTTP traffic with request and response bodies
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    
    // Log debugging info about Core Data
    RKLogConfigureByName("RestKit/CoreData", RKLogLevelDebug);
#endif


    // Define default headers
    AFHTTPClient* client = objectManager.HTTPClient;
    [client setDefaultHeader:@"Content-Type" value:@"application/json"];
    [client setDefaultHeader:@"Accept" value:@"application/json"];
    [client setDefaultHeader:HTTP_HEADER_ACCEPT_LANGUAGE value:[[NSLocale preferredLanguages] objectAtIndex:0]];
    
    //[RKObjectMapping setPreferredDateFormatter:[[BSLStyleSheet instance] dateFormatter]];
    objectManager.managedObjectStore = managedObjectStore;
    
    // enable progress indication when accessing content in web
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;*/
}

- (void)initAppearance
{
    
    if (![UISwitch respondsToSelector:@selector(appearance)]) {
        // iOS < 5.0
        return;
    }
}

- (void) resetApp
{
    // steps to reset the app completly
    //[[BundleSettings instance]  setServerUrl:nil];
}

#ifdef FEATURE_CHOOSESERVERADDRESS
- (void) chooseServerAddress
{
    /*
    NSArray* serverAddresses = (NSArray*) BSStaticTargetConfig(@"TestServerAddresses");
    if(serverAddresses.count <= 1) {
        // don't show chooser when only 1 server address is given
        return;
    }
    RIButtonItem *endItem = [RIButtonItem item];
    endItem.label = common_button_cancel_str;
    
    UIActionSheet* actionsheet = [[UIActionSheet alloc] initWithTitle:common_attention_str cancelButtonItem:endItem destructiveButtonItem:nil otherButtonItems: nil];
    
    for (int i = 0; i < serverAddresses.count; i++) {
        NSString* serverAddress = [serverAddresses objectAtIndex:i];
        RIButtonItem *item = [RIButtonItem item];
        item.label = serverAddress;
        item.action = ^
        {
            [self resetApp];
            [[BundleSettings instance]  setServerUrl:serverAddress];
            exit(0);
            
        };
        [actionsheet addButtonItem:item];
        
    }
    
    
    [actionsheet showInView:self.window];
     */
}

#endif //FEATURE_CHOOSESERVERADDRESS


@end
