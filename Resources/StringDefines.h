#ifndef StringDefines_h
#define StringDefines_h

#define common_alert_gps_error_msg_str NSLocalizedString(@"common_alert_gps_error_msg", @"")
#define common_loading_str NSLocalizedString(@"common_loading", @"")
#define common_error_str NSLocalizedString(@"common_error", @"")
#define common_placeholder_search_str NSLocalizedString(@"common_placeholder_search", @"")
#define common_button_cancel_str NSLocalizedString(@"common_button_cancel", @"")
#define common_facebook_str NSLocalizedString(@"common_facebook", @"")
#define common_twitter_str NSLocalizedString(@"common_twitter", @"")
#define common_email_str NSLocalizedString(@"common_email", @"")
#define common_optional_str NSLocalizedString(@"common_optional", @"")
#define common_attention_str NSLocalizedString(@"common_attention", @"")

#endif