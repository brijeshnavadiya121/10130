SDK    = "6.1"
SIMULATOR_DIR = "/Users/#{ENV['USER']}/Library/Application Support/iPhone Simulator/#{SDK}"
APPLICATIONS_DIR = "#{SIMULATOR_DIR}/Applications"
DCIM_DIR = "#{SIMULATOR_DIR}/Media/DCIM/100APPLE"
USERDEFAULTS_PLIST = "Library/Preferences/at.bluesource.app.enterprise.frankified.plist"
TEST_DATA_WITH_DB = "features/support/test_data/test_db/Documents"
TEST_DATA_INITIALIZED = "features/support/test_data/initialized/Documents"
DATA_PLIST = "Documents/data.plist"
DATABASE_PLIST = "Documents/Database.sqlite"
TEST_IMAGES = "features/support/test_data/image_for_photo_picker/"

Given /^I reset the iphone app$/ do
    steps "When I quit the simulator"

    Dir.foreach(APPLICATIONS_DIR) do |item|
        next if item == '.' or item == '..'
        if File::exists?("#{APPLICATIONS_DIR}/#{item}/#{USERDEFAULTS_PLIST}")
            FileUtils.rm "#{APPLICATIONS_DIR}/#{item}/#{USERDEFAULTS_PLIST}"
            FileUtils.rm "#{APPLICATIONS_DIR}/#{item}/#{DATA_PLIST}"
            FileUtils.rm "#{APPLICATIONS_DIR}/#{item}/#{DATABASE_PLIST}"
            FileUtils.cp Dir["#{TEST_DATA_INITIALIZED}/*"], "#{APPLICATIONS_DIR}/#{item}/Documents" 
        end
    end

end

Given /^there is a prefilled database$/ do
    steps "When I quit the simulator"

    Dir.foreach(APPLICATIONS_DIR) do |item|
        next if item == '.' or item == '..'
        if File::exists?("#{APPLICATIONS_DIR}/#{item}/#{USERDEFAULTS_PLIST}")
            FileUtils.cp Dir["#{TEST_DATA_WITH_DB}/*"], "#{APPLICATIONS_DIR}/#{item}/Documents"
        end
    end
end

Given /^there are images to pick with the image picker$/ do
    steps "When I quit the simulator"
    
    Dir.foreach(APPLICATIONS_DIR) do |item|
        next if item == '.' or item == '..'
        if File::exists?("#{APPLICATIONS_DIR}/#{item}/#{USERDEFAULTS_PLIST}")
        	FileUtils.mkdir_p "#{DCIM_DIR}"
        	FileUtils.cp "#{TEST_IMAGES}/image.JPG", "#{DCIM_DIR}"
        end
    end
end